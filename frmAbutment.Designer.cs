﻿namespace CBridge
{
    partial class frmAbutment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAbutment = new System.Windows.Forms.DataGridView();
            this.InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SeatTypes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.FixityTypes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Skew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ky = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Krx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Krz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lMateraial = new System.Windows.Forms.Label();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bReplicate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbutment)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAbutment
            // 
            this.dgvAbutment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAbutment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAbutment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InName,
            this.name,
            this.InUse,
            this.SeatTypes,
            this.FixityTypes,
            this.Skew,
            this.Kx,
            this.Ky,
            this.Kz,
            this.Krx,
            this.Kry,
            this.Krz});
            this.dgvAbutment.Location = new System.Drawing.Point(15, 32);
            this.dgvAbutment.Name = "dgvAbutment";
            this.dgvAbutment.Size = new System.Drawing.Size(797, 317);
            this.dgvAbutment.TabIndex = 0;
            this.dgvAbutment.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAbutment_CellValueChanged);
            this.dgvAbutment.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvAbutment_RowsAdded);
            this.dgvAbutment.SelectionChanged += new System.EventHandler(this.dgvAbutment_SelectionChanged);
            // 
            // InName
            // 
            this.InName.Frozen = true;
            this.InName.HeaderText = "InName";
            this.InName.Name = "InName";
            this.InName.ReadOnly = true;
            this.InName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.InName.Visible = false;
            // 
            // name
            // 
            this.name.Frozen = true;
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // InUse
            // 
            this.InUse.Frozen = true;
            this.InUse.HeaderText = "InUse";
            this.InUse.Name = "InUse";
            this.InUse.Width = 50;
            // 
            // SeatTypes
            // 
            this.SeatTypes.Frozen = true;
            this.SeatTypes.HeaderText = "Seat Type";
            this.SeatTypes.Items.AddRange(new object[] {
            "Seat",
            "Diaphragm"});
            this.SeatTypes.Name = "SeatTypes";
            this.SeatTypes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SeatTypes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FixityTypes
            // 
            this.FixityTypes.Frozen = true;
            this.FixityTypes.HeaderText = "Fixity Type";
            this.FixityTypes.Items.AddRange(new object[] {
            "Spring",
            "Slider",
            "Roller",
            "Pin",
            "Pin+Torsion",
            "Fix"});
            this.FixityTypes.Name = "FixityTypes";
            this.FixityTypes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FixityTypes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Skew
            // 
            this.Skew.Frozen = true;
            this.Skew.HeaderText = "Skew";
            this.Skew.Name = "Skew";
            // 
            // Kx
            // 
            this.Kx.FillWeight = 50F;
            this.Kx.HeaderText = "Kx";
            this.Kx.Name = "Kx";
            this.Kx.Width = 50;
            // 
            // Ky
            // 
            this.Ky.FillWeight = 50F;
            this.Ky.HeaderText = "Ky";
            this.Ky.Name = "Ky";
            this.Ky.Width = 50;
            // 
            // Kz
            // 
            this.Kz.FillWeight = 50F;
            this.Kz.HeaderText = "Kz";
            this.Kz.Name = "Kz";
            this.Kz.Width = 50;
            // 
            // Krx
            // 
            this.Krx.FillWeight = 50F;
            this.Krx.HeaderText = "Krx";
            this.Krx.Name = "Krx";
            this.Krx.Width = 50;
            // 
            // Kry
            // 
            this.Kry.FillWeight = 50F;
            this.Kry.HeaderText = "Kry";
            this.Kry.Name = "Kry";
            this.Kry.Width = 50;
            // 
            // Krz
            // 
            this.Krz.FillWeight = 50F;
            this.Krz.HeaderText = "Krz";
            this.Krz.Name = "Krz";
            this.Krz.Width = 50;
            // 
            // lMateraial
            // 
            this.lMateraial.AutoSize = true;
            this.lMateraial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMateraial.ForeColor = System.Drawing.Color.DarkRed;
            this.lMateraial.Location = new System.Drawing.Point(12, 14);
            this.lMateraial.Name = "lMateraial";
            this.lMateraial.Size = new System.Drawing.Size(141, 15);
            this.lMateraial.TabIndex = 39;
            this.lMateraial.Text = "Abutment Properties:";
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "lb, in",
            "lb, ft",
            "kip, in",
            "kip, ft"});
            this.cmbUnit.Location = new System.Drawing.Point(563, 379);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(70, 23);
            this.cmbUnit.TabIndex = 44;
            this.cmbUnit.Text = "kip, in";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.bCancel.Location = new System.Drawing.Point(714, 374);
            this.bCancel.Margin = new System.Windows.Forms.Padding(2);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(73, 28);
            this.bCancel.TabIndex = 43;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOK.ForeColor = System.Drawing.Color.DarkRed;
            this.bOK.Location = new System.Drawing.Point(637, 374);
            this.bOK.Margin = new System.Windows.Forms.Padding(2);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(73, 28);
            this.bOK.TabIndex = 42;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDelete.ForeColor = System.Drawing.Color.DarkRed;
            this.bDelete.Location = new System.Drawing.Point(130, 374);
            this.bDelete.Margin = new System.Windows.Forms.Padding(2);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(73, 28);
            this.bDelete.TabIndex = 41;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bReplicate
            // 
            this.bReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReplicate.ForeColor = System.Drawing.Color.DarkRed;
            this.bReplicate.Location = new System.Drawing.Point(35, 374);
            this.bReplicate.Margin = new System.Windows.Forms.Padding(2);
            this.bReplicate.Name = "bReplicate";
            this.bReplicate.Size = new System.Drawing.Size(73, 28);
            this.bReplicate.TabIndex = 40;
            this.bReplicate.TabStop = false;
            this.bReplicate.Text = "Replicate";
            this.bReplicate.UseVisualStyleBackColor = true;
            this.bReplicate.Click += new System.EventHandler(this.bReplicate_Click);
            // 
            // frmAbutment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 435);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bReplicate);
            this.Controls.Add(this.lMateraial);
            this.Controls.Add(this.dgvAbutment);
            this.Name = "frmAbutment";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbutment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAbutment;
        private System.Windows.Forms.Label lMateraial;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bReplicate;
        private System.Windows.Forms.DataGridViewTextBoxColumn InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn InUse;
        private System.Windows.Forms.DataGridViewComboBoxColumn SeatTypes;
        private System.Windows.Forms.DataGridViewComboBoxColumn FixityTypes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Skew;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ky;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Krx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Krz;
    }
}