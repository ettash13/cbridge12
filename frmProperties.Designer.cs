﻿namespace CBridge
{
    partial class frmProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvProp = new System.Windows.Forms.DataGridView();
            this.clmProperty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmValuesGirderOnly = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProp)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvProp
            // 
            this.dgvProp.AllowUserToAddRows = false;
            this.dgvProp.AllowUserToDeleteRows = false;
            this.dgvProp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmProperty,
            this.clmValuesGirderOnly});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProp.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProp.Location = new System.Drawing.Point(12, 13);
            this.dgvProp.Name = "dgvProp";
            this.dgvProp.ReadOnly = true;
            this.dgvProp.RowHeadersVisible = false;
            this.dgvProp.Size = new System.Drawing.Size(163, 199);
            this.dgvProp.TabIndex = 31;
            // 
            // clmProperty
            // 
            this.clmProperty.HeaderText = "Prop";
            this.clmProperty.Name = "clmProperty";
            this.clmProperty.ReadOnly = true;
            this.clmProperty.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmProperty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmProperty.Width = 80;
            // 
            // clmValuesGirderOnly
            // 
            dataGridViewCellStyle2.Format = "N2";
            this.clmValuesGirderOnly.DefaultCellStyle = dataGridViewCellStyle2;
            this.clmValuesGirderOnly.HeaderText = "Value";
            this.clmValuesGirderOnly.Name = "clmValuesGirderOnly";
            this.clmValuesGirderOnly.ReadOnly = true;
            this.clmValuesGirderOnly.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmValuesGirderOnly.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmValuesGirderOnly.Width = 80;
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnit.Location = new System.Drawing.Point(136, 218);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(38, 21);
            this.cmbUnit.TabIndex = 109;
            this.cmbUnit.SelectedIndexChanged += new System.EventHandler(this.cmbUnit_SelectedIndexChanged);
            this.cmbUnit.SelectedValueChanged += new System.EventHandler(this.cmbUnit_SelectedValueChanged);
            // 
            // frmProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(185, 251);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.dgvProp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProperties";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Properties";
            this.Load += new System.EventHandler(this.frmProperties_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn clmProperty;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmValuesGirderOnly;
        private System.Windows.Forms.ComboBox cmbUnit;
        public System.Windows.Forms.DataGridView dgvProp;
    }
}