﻿namespace CBridge
{
    partial class frmBent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lMateraial = new System.Windows.Forms.Label();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bReplicate = new System.Windows.Forms.Button();
            this.dgvBent = new System.Windows.Forms.DataGridView();
            this.InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Skew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sections = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Columns = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBent)).BeginInit();
            this.SuspendLayout();
            // 
            // lMateraial
            // 
            this.lMateraial.AutoSize = true;
            this.lMateraial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMateraial.ForeColor = System.Drawing.Color.DarkRed;
            this.lMateraial.Location = new System.Drawing.Point(12, 9);
            this.lMateraial.Name = "lMateraial";
            this.lMateraial.Size = new System.Drawing.Size(110, 15);
            this.lMateraial.TabIndex = 39;
            this.lMateraial.Text = "Bent Properties:";
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "lb, in",
            "lb, ft",
            "kip, in",
            "kip, ft"});
            this.cmbUnit.Location = new System.Drawing.Point(491, 381);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(70, 23);
            this.cmbUnit.TabIndex = 44;
            this.cmbUnit.Text = "kip, in";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.bCancel.Location = new System.Drawing.Point(656, 376);
            this.bCancel.Margin = new System.Windows.Forms.Padding(2);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(73, 28);
            this.bCancel.TabIndex = 43;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOK.ForeColor = System.Drawing.Color.DarkRed;
            this.bOK.Location = new System.Drawing.Point(570, 376);
            this.bOK.Margin = new System.Windows.Forms.Padding(2);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(73, 28);
            this.bOK.TabIndex = 42;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDelete.ForeColor = System.Drawing.Color.DarkRed;
            this.bDelete.Location = new System.Drawing.Point(130, 376);
            this.bDelete.Margin = new System.Windows.Forms.Padding(2);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(73, 28);
            this.bDelete.TabIndex = 41;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bReplicate
            // 
            this.bReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReplicate.ForeColor = System.Drawing.Color.DarkRed;
            this.bReplicate.Location = new System.Drawing.Point(35, 376);
            this.bReplicate.Margin = new System.Windows.Forms.Padding(2);
            this.bReplicate.Name = "bReplicate";
            this.bReplicate.Size = new System.Drawing.Size(73, 28);
            this.bReplicate.TabIndex = 40;
            this.bReplicate.TabStop = false;
            this.bReplicate.Text = "Replicate";
            this.bReplicate.UseVisualStyleBackColor = true;
            this.bReplicate.Click += new System.EventHandler(this.bReplicate_Click);
            // 
            // dgvBent
            // 
            this.dgvBent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InName,
            this.name,
            this.InUse,
            this.Type,
            this.Skew,
            this.CGOffset,
            this.Sections,
            this.Columns});
            this.dgvBent.Location = new System.Drawing.Point(12, 37);
            this.dgvBent.Name = "dgvBent";
            this.dgvBent.Size = new System.Drawing.Size(746, 318);
            this.dgvBent.TabIndex = 45;
            // 
            // InName
            // 
            this.InName.HeaderText = "InName";
            this.InName.Name = "InName";
            this.InName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.InName.Visible = false;
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // InUse
            // 
            this.InUse.HeaderText = "InUse";
            this.InUse.Name = "InUse";
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Items.AddRange(new object[] {
            "Monolithic",
            "Drop Cap",
            "Pin-Pin",
            "Roller-Pin",
            "Pin-Roller",
            "Cont/Pin",
            "Cont/Roller",
            "SDL/CLL"});
            this.Type.Name = "Type";
            this.Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Skew
            // 
            this.Skew.HeaderText = "Skew";
            this.Skew.Name = "Skew";
            // 
            // CGOffset
            // 
            this.CGOffset.HeaderText = "CGOffset";
            this.CGOffset.Name = "CGOffset";
            // 
            // Sections
            // 
            this.Sections.HeaderText = "Sections";
            this.Sections.Name = "Sections";
            this.Sections.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Sections.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Columns
            // 
            this.Columns.HeaderText = "Columns";
            this.Columns.Name = "Columns";
            this.Columns.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Columns.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frmBent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 429);
            this.Controls.Add(this.dgvBent);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bReplicate);
            this.Controls.Add(this.lMateraial);
            this.Name = "frmBent";
            this.Text = "frmBent";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lMateraial;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bReplicate;
        private System.Windows.Forms.DataGridView dgvBent;
        private System.Windows.Forms.DataGridViewTextBoxColumn InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn InUse;
        private System.Windows.Forms.DataGridViewComboBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Skew;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGOffset;
        private System.Windows.Forms.DataGridViewComboBoxColumn Sections;
        private System.Windows.Forms.DataGridViewComboBoxColumn Columns;
    }
}