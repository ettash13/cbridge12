﻿/*
 * Math and Matrix operation static functions
 * Initial Development: Toorak Zokaie, 2016-06-01
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBridge
{
    /// <summary>
    /// Math and Matrix operation static functions
    /// </summary>
    public static class CTMath
    {
        /// <summary>
        /// Return a copy of the array
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static int[] Duplicate(int[] A)
        {
            int[] B = null;
            if (A != null)
            {
                B = new int[A.Length];
                for (int i = 0; i < A.Length; i++) B[i] = A[i];
            }
            return B;
        }

        /// <summary>
        /// Return a copy of the array
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool[] Duplicate(bool[] A)
        {
            bool[] B = null;
            if (A != null)
            {
                B = new bool[A.Length];
                for (int i = 0; i < A.Length; i++) B[i] = A[i];
            }
            return B;
        }
        public static double[] Duplicate(double[] A)
        {
            double[] B = null;
            if (A != null)
            {
                B = new double[A.Length];
                for (int i = 0; i < A.Length; i++) B[i] = A[i];
            }
            return B;
        }
        /// <summary>
        /// Return a copy of the array
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static string[] Duplicate(string[] A)
        {
            string[] B = null;
            if (A != null)
            {
                B = new string[A.Length];
                for (int i = 0; i < A.Length; i++) B[i] = A[i];
            }
            return B;
        }
        /// <summary>
        /// Return a copy of the array
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double[,] Duplicate(double[,] A)
        {
            double[,] B = null;
            if (A != null)
            {
                B = new double[A.GetLength(0), A.GetLength(1)];
                for (int i = 0; i < A.GetLength(0); i++)
                {
                    for (int j = 0; j < A.GetLength(1); j++)
                    {
                        B[i, j] = A[i, j];
                    }
                }
            }
            return B;
        }

        /// <summary>
        /// Transpose of a 2D matrix
        /// </summary>
        /// <param name="A">matrix to be transposed</param>
        /// <returns>Transpose of A</returns>
        public static double[,] MTranspose(double[,] A)
        {
            int NRow = A.GetLength(0);
            int nCol = A.GetLength(1);
            double[,] B = new double[nCol, NRow];
            for (int i = 0; i < NRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    B[j, i] = A[i, j];
                }
            }
            return B;
        }

        /// <summary>
        /// Multiply Matrices: C = A*B
        /// </summary>
        /// <param name="A">First matrix</param>
        /// <param name="B">Second Matrix</param>
        /// <param name="RetCode">Note use as input: true=success, false=failed</param>
        /// <returns>The resulting matrix</returns>
        public static double[,] MMult(double[,] A, double[,] B, bool RetCode = true)
        {
            int M = A.GetLength(0);
            int N = A.GetLength(1);
            if (N != B.GetLength(0)) // Cannot Multiply
            {
                RetCode = false;
                return null;
            }
            int P = B.GetLength(1);
            double[,] C = new double[M, P];
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < P; j++)
                {
                    C[i, j] = 0.0;
                    for (int k = 0; k < N; k++)
                    {
                        C[i, j] += A[i, k] * B[k, j];
                    }
                }
            }
            return C;
        }
        public static double[] MMult(double[,] A, double[] B, bool RetCode = true)
        {

            int M = A.GetLength(0);
            int N = A.GetLength(1);
            if (N != B.GetLength(0)) // Cannot Multiply
            {
                RetCode = false;
                return null;
            }
            double[] C = new double[M];
            for (int i = 0; i < M; i++)
            {
                C[i] = 0.0;
                for (int k = 0; k < N; k++)
                {
                    C[i] += A[i, k] * B[k];
                }
            }
            return C;
        }

        /// <summary>
        /// Solves a system linear equations for a number of right hand sides: CX=B
        /// </summary>
        /// <param name="C">Coeffeicient matrix [N,N]</param>
        /// <param name="B">Right hand sides [N,nset]</param>
        /// <param name="RetCode">Note use as input: true=success, false=failed</param>
        /// <returns>the solution (X) [N,nset]</returns>
        public static double[,] MSolve(double[,] C, double[,] B, bool RetCode = true)
        {
            double[,] X = null;
            try
            {
                int M = C.GetLength(0);
                int N = C.GetLength(1);
                if (M != N) // Cannot Solve
                {
                    RetCode = false;
                    return null;
                }
                if (N != B.GetLength(0)) // Cannot Solve
                {
                    RetCode = false;
                    return null;
                }
                int NSet = B.GetLength(1);

                X = Duplicate(B);
                double[,] A = Duplicate(C);

                //Forward Reduction
                for (int i = 0; i < N - 1; i++)
                {
                    for (int j = i + 1; j < N; j++)
                    {
                        if (A[i, i] != 0)  // If diagonal is 0, skip this equation
                        {
                            double r = -A[j, i] / A[i, i]; // Scale factor for row j
                            if (r != 0)
                            { // If the row is already 0 at this column, no need to reduce
                                for (int k = 0; k < N; k++)
                                {
                                    A[j, k] += r * A[i, k];
                                }
                                for (int l = 0; l < NSet; l++)
                                {
                                    X[j, l] += r * X[i, l];
                                }
                            }
                        }
                    }
                }
                //Back Substitution, i.e., solution of the remaining single equations
                for (int i = N - 1; i >= 0; i--)
                {
                    if (A[i, i] == 0)
                    {
                        for (int k = 0; k < NSet; k++) X[i, k] = 0;  // Equation is invalid
                    }
                    else
                    {
                        for (int j = N - 1; j >= i + 1; j--)
                        {
                            for (int k = 0; k < NSet; k++)
                            {
                                X[i, k] -= A[i, j] * X[j, k];
                            }
                        }
                        for (int k = 0; k < NSet; k++)
                        {
                            X[i, k] = X[i, k] / A[i, i];
                        }
                    }
                }
                RetCode = true;
            }
            catch
            {
                RetCode = false;
                return null;
            }
            return X;
        }

        /// <summary>
        /// Interpolate bewteen two points: y = y1 + (x - x1) * (y2 - y1) / (x2 - x1)
        /// </summary>
        /// <param name="x1">x of the first point</param>
        /// <param name="y1">y of the first point</param>
        /// <param name="x2">x of the second point</param>
        /// <param name="y2">y of the second point</param>
        /// <param name="x">x of the point  for which "y" is calculated</param>
        /// <returns>y of x</returns>
        public static double Interpolate(double x1, double y1, double x2, double y2, double x)
        {
            if (x1 == x2) return 0.5 * (y1 + y2); else return y1 + (x - x1) * (y2 - y1) / (x2 - x1);
        }

    }

}
