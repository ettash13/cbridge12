﻿using System;
using System.Linq;
using System.Data;
using System.Xml;
using System.Globalization;
using ZoconFEA3D;      // Add for anything that needs to access m_Structure or core functions


namespace CBridge
{
    class CBridgeIO
    {
        // Save to XML Section
 #region  SaveSection

        /// <summary>
        /// 
        /// saveInputLiraryToXML
        /// 
        /// 
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        public void saveInputLiraryToXML(InputLibrary m_InputLibrary, string FileName)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            DateTime localDate = DateTime.Now;

            if (FileName == null)
            {
                FileName = "CbridgeData.XML";
            }
            
            writer = XmlWriter.Create(FileName, settings);
            writer.WriteComment("Savind CBridge data from  : " + localDate.ToString());

            // Write the root element
            writer.WriteStartElement("BridgeInput");

            if (m_InputLibrary.Materials != null)
            {
                // add material to XML 
                int materialCount = m_InputLibrary.Materials.Count();

                if (m_InputLibrary.Materials.Count() > 0)
                {
                    saveMaterialsToXML(m_InputLibrary.Materials, writer);
                }
            }

            // Write Alignmnets to XML
            if (m_InputLibrary.Alignments != null)
            {
                if (m_InputLibrary.Alignments.Count() > 0)
                {
                    saveAlignmentsToXML(m_InputLibrary.Alignments, writer);
                }
            }

            // Write Abutments to XML
            if (m_InputLibrary.Abutments != null)
            {
                if (m_InputLibrary.Abutments.Count() > 0)
                {
                    saveAbutmentsToXML(m_InputLibrary.Abutments, writer);
                }
            }

            // Write Columns to XML
            if (m_InputLibrary.Columns != null)
            {
                if (m_InputLibrary.Columns.Count() > 0)
                {
                    saveColumnsToXML(m_InputLibrary.Columns, writer);
                }
            }

            // Write CrossSections to XML
            if (m_InputLibrary.CrossSections != null)
            {
                if (m_InputLibrary.CrossSections.Count() > 0)
                {
                    saveCrossSectionsToXML(m_InputLibrary.CrossSections, writer);
                }
            }

            // Write Bents to XML
            if (m_InputLibrary.Bents != null)
            {
                if (m_InputLibrary.Bents.Count() > 0)
                {
                    saveBentsToXML(m_InputLibrary.Bents, writer);
                }
            }

            // Write END to XML
            writer.WriteEndDocument();

            writer.Flush();
            writer.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveMaterialsToXML(Material[] Materials, XmlWriter writer)
        {
            // Write Material elements 
            writer.WriteStartElement("Materials");

            foreach (Material item in Materials)
            {
                writer.WriteStartElement("Material");

                writer.WriteAttributeString("E", item.E.ToString());
                writer.WriteAttributeString("UnitWeight", item.gamma.ToString());
                writer.WriteAttributeString("PR", item.v.ToString());

                if (item.GetType() == typeof(MaterialConc))
                {
                    writer.WriteAttributeString("fc", ((MaterialConc)item).fc.ToString());
                    writer.WriteAttributeString("fci", ((MaterialConc)item).fci.ToString());
                    writer.WriteAttributeString("type", "Concrete");
                }
                else if (item.GetType() == typeof(MaterialSteel))
                {
                    writer.WriteAttributeString("fy", ((MaterialSteel)item).fy.ToString());
                    writer.WriteAttributeString("fu", ((MaterialSteel)item).fu.ToString());
                    writer.WriteAttributeString("Grade", ((MaterialSteel)item).E.ToString());
                    writer.WriteAttributeString("type", ((MaterialSteel)item).type);
                }
                else
                {
                    writer.WriteAttributeString("type", "Elastic");
                }

                writer.WriteString(item.MaterialName);
                writer.WriteEndElement();
            }

            // end "Materials"
            writer.WriteEndElement();
        }
                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveAlignmentsToXML(Alignment[] Alignments, XmlWriter writer)
        {
            Alignment item = Alignments[0];

            // Write an element (this one is the root).
            writer.WriteStartElement("Alignments");

            writer.WriteStartElement("Alignment");
            writer.WriteAttributeString("StartX", item.XYZ0[0].ToString());
            writer.WriteAttributeString("StartY", item.XYZ0[1].ToString());
            writer.WriteAttributeString("StartZ", item.XYZ0[2].ToString());
            writer.WriteAttributeString("AzStart", item.CrvAzStart[0].ToString());
            writer.WriteAttributeString("VSlopeStart", item.m_VSlopeStart.ToString());  // must use get set
            writer.WriteAttributeString("type", "Start");
            writer.WriteEndElement();

            // do horizontal Curves, one lone per
            for (int i = 0; i < item.CrvRadius.Length; i++)
            {
                writer.WriteStartElement("Alignment");
                writer.WriteAttributeString("Radius", item.CrvRadius[i].ToString());
                writer.WriteAttributeString("Length", item.CrvLength[i].ToString());
                writer.WriteAttributeString("type", "Horizontal");
                writer.WriteEndElement();
            }

            // do horizontal Curves, one lone per
            for (int i = 0; i < item.m_VCrvRun.Length; i++)
            {
                writer.WriteStartElement("Alignment");
                writer.WriteAttributeString("Rise", item.m_VCrvRise[i].ToString());
                writer.WriteAttributeString("Run", item.m_VCrvRun[i].ToString());
                writer.WriteAttributeString("type", "Vertical");
                writer.WriteEndElement();
            }
            
            // end "Alignments"
            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveAbutmentsToXML(Abutment[] Abutments, XmlWriter writer)
        {
            writer.WriteStartElement("Abutments");

            foreach (Abutment item in Abutments)
            {
                writer.WriteStartElement("Abutment");

                writer.WriteAttributeString("Skew", item.Skew.ToString());

                writer.WriteAttributeString("SeatType", item.Seattype.ToString());
                writer.WriteAttributeString("FixityType", item.Fixitytype.ToString());

                writer.WriteAttributeString("Kx", item.Kx.ToString());
                writer.WriteAttributeString("Ky", item.Ky.ToString());
                writer.WriteAttributeString("Kz", item.Kz.ToString());
             
                writer.WriteAttributeString("Krx", item.Krx.ToString());
                writer.WriteAttributeString("Kry", item.Kry.ToString());
                writer.WriteAttributeString("Krz", item.Krz.ToString());
                
                writer.WriteString(item.DisplayName);
                writer.WriteEndElement();
            }

            // end "Abutments"
            writer.WriteEndElement();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveCrossSectionsToXML(CrossSection[] CrossSections, XmlWriter writer)
        {
            // Write Material elements 
            writer.WriteStartElement("CrossSections");

            foreach (CrossSection item in CrossSections)
            {
                writer.WriteStartElement("CrossSection");
                writer.WriteAttributeString("Shape", item.SecShape());
                writer.WriteAttributeString("Type", item.SecType());

                writer.WriteString(item.DisplayName);
                writer.WriteEndElement();
            }

            // end "CrossSections"
            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveColumnsToXML(Column[] Columns, XmlWriter writer)
        {
            // Write Material elements 
            writer.WriteStartElement("Columns");

            foreach (Column item in Columns)
            {
                writer.WriteStartElement("Column");
                writer.WriteAttributeString("Distance", item.Distance.ToString());
                writer.WriteAttributeString("Length", item.Length.ToString());
                writer.WriteAttributeString("Base", item.Base.ToString());
                writer.WriteAttributeString("Top", item.Top.ToString());
                writer.WriteAttributeString("Section", item.Section.ToString());
                writer.WriteString(item.Name);
                writer.WriteEndElement();
            }

            // end "Columns"
            writer.WriteEndElement();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveBentsToXML(Bent[] Bents, XmlWriter writer)
        {
            // Write Material elements 
            writer.WriteStartElement("Bents");

            foreach (Bent item in Bents)
            {
                writer.WriteStartElement("Bent");
                writer.WriteAttributeString("Type", item.type.ToString()); 
                writer.WriteAttributeString("Skew", item.Skew.ToString());
                writer.WriteAttributeString("CGOffset", item.cgOffset.ToString());
                writer.WriteAttributeString("Sections", item.sectionName.ToString());
                writer.WriteAttributeString("Columns", item.columnName.ToString());
                writer.WriteString(item.Name);
                writer.WriteEndElement();
            }

            // end "Bents"
            writer.WriteEndElement();
        }
        
#endregion SaveSection

        // Restore from XML Section
#region RestoreFromXMLSection

        /// <summary>
        ///  Read all saved parameters from XML file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        public void fillInputLiraryFromXML(InputLibrary m_InputLibrary, string FileName)
        {
            
            XmlDocument doc = new XmlDocument();
            doc.Load(FileName);

            XmlNodeList elemList = doc.GetElementsByTagName("Material");

            if (elemList.Count > 0)
            {
                fillMaterials(ref m_InputLibrary.Materials, elemList);
            }

            elemList = doc.GetElementsByTagName("Alignment");
            if (elemList.Count > 0)
            {
                fillAlignments(ref m_InputLibrary.Alignments, elemList);
            }

            elemList = doc.GetElementsByTagName("Abutment");
            if (elemList.Count > 0)
            {
                fillAbutments(ref m_InputLibrary.Abutments, elemList);
            }

            elemList = doc.GetElementsByTagName("CrossSection");
            if (elemList.Count > 0)
            {
                fillCrossSections(ref m_InputLibrary.CrossSections, elemList);
            }

            elemList = doc.GetElementsByTagName("Column");
            if (elemList.Count > 0)
            {
                fillColumns(ref m_InputLibrary.Columns, elemList);
            }
            
            elemList = doc.GetElementsByTagName("Bent");
            if (elemList.Count > 0)
            {
                fillBents(ref m_InputLibrary.Bents, elemList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillMaterials(ref Material[] Materials, XmlNodeList elemList)
        { 
            // update input library Materials from xml file
            Materials = new Material[elemList.Count];

            string MatType = "";
            string MatName = "";
            double[] Prop = new double[6];
            int i = 0;

            foreach (XmlNode node in elemList)
            {
                MatType = node.Attributes["type"].Value;
                MatName = node.InnerText;

                Prop[0] = Convert.ToDouble(node.Attributes["E"].Value);
                Prop[1] = Convert.ToDouble(node.Attributes["UnitWeight"].Value);
                Prop[2] = Convert.ToDouble(node.Attributes["PR"].Value);

                if (MatType == "Concrete")
                {
                    Prop[3] = Convert.ToDouble(node.Attributes["fc"].Value);
                    Prop[4] = Convert.ToDouble(node.Attributes["fci"].Value);
                }
                else if ((MatType == "Mild") || (MatType == "Strand"))
                {
                    Prop[3] = Convert.ToDouble(node.Attributes["fy"].Value);
                    Prop[4] = Convert.ToDouble(node.Attributes["fu"].Value);
                    Prop[5] = Convert.ToDouble(node.Attributes["Grade"].Value);
                }

                //////////////////////////////////////////////////////////
                //
                // update input Library
                //
                //////////////////////////////////////////////////////////
                if (MatType == "Elastic")
                {
                    Material elastic = new Material(Prop[0], Prop[1], Prop[2], MatName);
                    elastic.DisplayName = MatName;

                    Materials[i++] = elastic;

                }
                else if (MatType == "Concrete")
                {
                    MaterialConc concrete = new MaterialConc(Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    concrete.DisplayName = MatName;

                    Materials[i++] = concrete;

                }
                else if (MatType == "Mild")
                {
                    MaterialSteel mildSteel = new MaterialSteel("Mild", Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    mildSteel.DisplayName = MatName;

                    Materials[i++] = mildSteel;

                }
                else if (MatType == "Strand")
                {
                    MaterialSteel strandSteel = new MaterialSteel("Strand", Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    strandSteel.DisplayName = MatName;

                    Materials[i++] = strandSteel;
                }
            }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillAlignments(ref Alignment[] Alignment, XmlNodeList elemList)
        {
            // update Alignments from xml file
            Alignment = new Alignment[1];

            string Name = "";
            double[] XYZStart = new double[3];
            double AzStart = 0;
            double VSlopeStart = 0;
            string Type = "";

            int ih = 0;
            int iv = 0;
            
            // count the number of horizontal and vertical rows in XML
            foreach (XmlNode node in elemList)
            {
                Type = node.Attributes["type"].Value;

                if (Type == "Horizontal")
                {
                    ih++;
                }
                else if (Type == "Vertical")
                {
                    iv++;
                }
            }
           
            double[] CrvRad    = new double[ih];
            double[] CrvLen    = new double[ih];
                      
            double[] VCrvRun   = new double[iv];
            double[] VCrvRise  = new double[iv];

            // now read XML nodes
            ih = iv = 0;

            foreach (XmlNode node in elemList)
            {
                Type = node.Attributes["type"].Value;

                if (Type == "Start")
                {
                    XYZStart[0] = Convert.ToDouble(node.Attributes["StartX"].Value);
                    XYZStart[1] = Convert.ToDouble(node.Attributes["StartY"].Value);
                    XYZStart[2] = Convert.ToDouble(node.Attributes["StartZ"].Value);

                    AzStart     = Convert.ToDouble(node.Attributes["AzStart"].Value);
                    VSlopeStart = Convert.ToDouble(node.Attributes["VSlopeStart"].Value);

                }
                else if (Type == "Horizontal")
                {
                    CrvRad[ih] = Convert.ToDouble(node.Attributes["Radius"].Value);
                    CrvLen[ih] = Convert.ToDouble(node.Attributes["Length"].Value);

                    ih++;

                }
                else if (Type == "Vertical")
                {
                    VCrvRun[iv]  = Convert.ToDouble(node.Attributes["Rise"].Value);
                    VCrvRise[iv] = Convert.ToDouble(node.Attributes["Run"].Value);

                    iv++;
                }
            }

            Alignment alignment = new Alignment(Name, XYZStart, AzStart, CrvRad, CrvLen);
            alignment.SetVerticalCurve(VSlopeStart, VCrvRun, VCrvRise);
            Alignment[0] = alignment;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillAbutments(ref Abutment[] Abutments, XmlNodeList elemList)
        {
            // update input library Materials from xml file
            Abutments = new Abutment[elemList.Count];

            string AbutmentName = "";
            
            string SeatType = "";
            string FixityType = "";
            double[] Prop = new double[7];
                        
            int i = 0;

            foreach (XmlNode node in elemList)
            {
                AbutmentName = node.InnerText;
                SeatType = node.Attributes["SeatType"].Value;
                FixityType = node.Attributes["FixityType"].Value;

                Prop[0] = Convert.ToInt32(node.Attributes["Skew"].Value);

                Prop[1] = Convert.ToInt32(node.Attributes["Kx"].Value);
                Prop[2] = Convert.ToInt32(node.Attributes["Ky"].Value);
                Prop[3] = Convert.ToInt32(node.Attributes["Kz"].Value);

                Prop[1] = Convert.ToInt32(node.Attributes["Krx"].Value);
                Prop[2] = Convert.ToInt32(node.Attributes["Kry"].Value);
                Prop[3] = Convert.ToInt32(node.Attributes["Krz"].Value);
                             
                Abutment Abut = new Abutment(AbutmentName, AbutmentName, null, AbutmentName, SeatType, FixityType, Prop);
                Abut.DisplayName = AbutmentName;
                Abutments[i++] = Abut;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillCrossSections(ref CrossSection[] Sections, XmlNodeList elemList)
        {
            // update input library Materials from xml file
            Sections  = new CrossSection[elemList.Count];

            string SectionName = "";
            string SectionShape = "";
            string SectionType = "";

            int i = 0;

            foreach (XmlNode node in elemList)
            {                      
                SectionName = node.InnerText;
                SectionShape = node.Attributes["Shape"].Value;
                SectionType = node.Attributes["Type"].Value;
                
                // Update input Library. need to be able to use SecShape. 10 and 10 are just random number now
                CrossSection Sec = new CrossSection(SectionType, 10, 10, SectionName);
                Sec.DisplayName = SectionName;
                Sections[i++] = Sec;                        
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillColumns(ref Column[] Columns, XmlNodeList elemList)
        {
            // update input library Materials from xml file
            Columns = new Column[elemList.Count];

            string ColumnName = "";
            double Distance = 0;
            double Length = 0;
            string Base = "";
            string Top = "";
            string Section = "";
        
            int i = 0;

            foreach (XmlNode node in elemList)
            {
                ColumnName = node.InnerText;

                Distance = Convert.ToInt32(node.Attributes["Distance"].Value);
                Length = Convert.ToInt32(node.Attributes["Length"].Value);

                Base = node.Attributes["Base"].Value;
                Top = node.Attributes["Top"].Value;
                Section = node.Attributes["Section"].Value;

                Column Column1 = new Column(ColumnName, Distance, Length, 0, Base, Top, Section);
                Column1.Name = ColumnName;
             
                Columns[i++] = Column1;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillBents(ref Bent[] Bents, XmlNodeList elemList)
        {
            // update input library Materials from xml file
            Bents = new Bent[elemList.Count];

            string BentName = "";
            string BentType = "";
            double Skew = 0;
            double CGOffset = 0;
            
            string SectionName = "";
            string ColumnName = "";
                       
            int i = 0;

            foreach (XmlNode node in elemList)
            {
                BentName = node.InnerText;
                BentType = node.Attributes["Type"].Value;
                Skew = Convert.ToInt32(node.Attributes["Skew"].Value);
                CGOffset = Convert.ToInt32(node.Attributes["CGOffset"].Value);
                SectionName = node.Attributes["Sections"].Value;
                ColumnName = node.Attributes["Columns"].Value;
                          
                Bent Bent1 = new Bent(BentName, BentType, Skew, CGOffset, SectionName, ColumnName);

                Bent1.Name = BentName;
                Bents[i++] = Bent1;
            }
        }

#endregion RestoreFromXMLSection
    }
}     