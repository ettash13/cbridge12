﻿namespace CBridge
{
    partial class frmAlignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvHoizontalSegments = new System.Windows.Forms.DataGridView();
            this.HDirectionCombo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Radius = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lenght = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvStartXYZ = new System.Windows.Forms.DataGridView();
            this.Start_X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Start_Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Start_Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Station = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AzimuthStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlopeStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReplicate = new System.Windows.Forms.Button();
            this.btnDeleteRow = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvVerticalSegments = new System.Windows.Forms.DataGridView();
            this.VDirectionCombo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Rise = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Run = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.tabAlignment = new System.Windows.Forms.TabControl();
            this.tabHorizontal = new System.Windows.Forms.TabPage();
            this.pbHView = new System.Windows.Forms.PictureBox();
            this.tabVertical = new System.Windows.Forms.TabPage();
            this.pbVView = new System.Windows.Forms.PictureBox();
            this.tab3D = new System.Windows.Forms.TabPage();
            this.pb3DView = new System.Windows.Forms.PictureBox();
            this.lMateraial = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoizontalSegments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStartXYZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVerticalSegments)).BeginInit();
            this.tabAlignment.SuspendLayout();
            this.tabHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHView)).BeginInit();
            this.tabVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVView)).BeginInit();
            this.tab3D.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb3DView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.btnCancel.Location = new System.Drawing.Point(775, 272);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(24, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Horizontal Segments";
            // 
            // dgvHoizontalSegments
            // 
            this.dgvHoizontalSegments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvHoizontalSegments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHoizontalSegments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HDirectionCombo,
            this.Radius,
            this.Lenght});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHoizontalSegments.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHoizontalSegments.Location = new System.Drawing.Point(26, 137);
            this.dgvHoizontalSegments.Name = "dgvHoizontalSegments";
            this.dgvHoizontalSegments.Size = new System.Drawing.Size(325, 195);
            this.dgvHoizontalSegments.TabIndex = 2;
            this.dgvHoizontalSegments.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHoizontalSegments_CellEndEdit);
            this.dgvHoizontalSegments.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHoizontalSegments_CellValueChanged);
            this.dgvHoizontalSegments.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgv_CellValueChanged);
            // 
            // HDirectionCombo
            // 
            this.HDirectionCombo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.HDirectionCombo.HeaderText = "H-Direction";
            this.HDirectionCombo.Items.AddRange(new object[] {
            "Left",
            "Straight",
            "Right"});
            this.HDirectionCombo.Name = "HDirectionCombo";
            this.HDirectionCombo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HDirectionCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.HDirectionCombo.ToolTipText = "Select horizontal direction ";
            // 
            // Radius
            // 
            this.Radius.HeaderText = "Radius";
            this.Radius.Name = "Radius";
            // 
            // Lenght
            // 
            this.Lenght.FillWeight = 80F;
            this.Lenght.HeaderText = "Lenght";
            this.Lenght.Name = "Lenght";
            // 
            // dgvStartXYZ
            // 
            this.dgvStartXYZ.AllowUserToDeleteRows = false;
            this.dgvStartXYZ.AllowUserToResizeRows = false;
            this.dgvStartXYZ.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStartXYZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStartXYZ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Start_X,
            this.Start_Y,
            this.Start_Z,
            this.Station,
            this.AzimuthStart,
            this.SlopeStart});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStartXYZ.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvStartXYZ.Location = new System.Drawing.Point(28, 58);
            this.dgvStartXYZ.Name = "dgvStartXYZ";
            this.dgvStartXYZ.Size = new System.Drawing.Size(703, 46);
            this.dgvStartXYZ.TabIndex = 3;
            this.dgvStartXYZ.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            // 
            // Start_X
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Start_X.DefaultCellStyle = dataGridViewCellStyle2;
            this.Start_X.HeaderText = "Start_X";
            this.Start_X.Name = "Start_X";
            // 
            // Start_Y
            // 
            this.Start_Y.HeaderText = "Start_Y";
            this.Start_Y.Name = "Start_Y";
            // 
            // Start_Z
            // 
            this.Start_Z.HeaderText = "Start_Z";
            this.Start_Z.Name = "Start_Z";
            // 
            // Station
            // 
            this.Station.HeaderText = "Station";
            this.Station.Name = "Station";
            this.Station.Visible = false;
            // 
            // AzimuthStart
            // 
            this.AzimuthStart.HeaderText = "Azimuth Start";
            this.AzimuthStart.Name = "AzimuthStart";
            // 
            // SlopeStart
            // 
            this.SlopeStart.HeaderText = "Starting Slope";
            this.SlopeStart.Name = "SlopeStart";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(23, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start XYZ";
            // 
            // btnReplicate
            // 
            this.btnReplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReplicate.ForeColor = System.Drawing.Color.DarkRed;
            this.btnReplicate.Location = new System.Drawing.Point(778, 140);
            this.btnReplicate.Name = "btnReplicate";
            this.btnReplicate.Size = new System.Drawing.Size(75, 23);
            this.btnReplicate.TabIndex = 6;
            this.btnReplicate.Text = "Replicate";
            this.btnReplicate.UseVisualStyleBackColor = true;
            this.btnReplicate.Click += new System.EventHandler(this.btnReplicate_Click);
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteRow.ForeColor = System.Drawing.Color.DarkRed;
            this.btnDeleteRow.Location = new System.Drawing.Point(778, 184);
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteRow.TabIndex = 7;
            this.btnDeleteRow.Text = "Delete";
            this.btnDeleteRow.UseVisualStyleBackColor = true;
            this.btnDeleteRow.Click += new System.EventHandler(this.btnDeleteRow_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(382, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Vertical Segments";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // dgvVerticalSegments
            // 
            this.dgvVerticalSegments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVerticalSegments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVerticalSegments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VDirectionCombo,
            this.Rise,
            this.Run});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVerticalSegments.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvVerticalSegments.Location = new System.Drawing.Point(386, 140);
            this.dgvVerticalSegments.Name = "dgvVerticalSegments";
            this.dgvVerticalSegments.Size = new System.Drawing.Size(345, 190);
            this.dgvVerticalSegments.TabIndex = 11;
            this.dgvVerticalSegments.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVerticalSegments_CellValueChanged);
            // 
            // VDirectionCombo
            // 
            this.VDirectionCombo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.VDirectionCombo.HeaderText = "V-Direction";
            this.VDirectionCombo.Items.AddRange(new object[] {
            "Down",
            "Straight",
            "Up"});
            this.VDirectionCombo.Name = "VDirectionCombo";
            this.VDirectionCombo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VDirectionCombo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.VDirectionCombo.ToolTipText = "Select vertical direction";
            // 
            // Rise
            // 
            this.Rise.HeaderText = "Rise";
            this.Rise.Name = "Rise";
            // 
            // Run
            // 
            this.Run.HeaderText = "Run";
            this.Run.Name = "Run";
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.DarkRed;
            this.btnOK.Location = new System.Drawing.Point(775, 228);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tabAlignment
            // 
            this.tabAlignment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabAlignment.Controls.Add(this.tabHorizontal);
            this.tabAlignment.Controls.Add(this.tabVertical);
            this.tabAlignment.Controls.Add(this.tab3D);
            this.tabAlignment.Location = new System.Drawing.Point(28, 347);
            this.tabAlignment.Name = "tabAlignment";
            this.tabAlignment.SelectedIndex = 0;
            this.tabAlignment.Size = new System.Drawing.Size(830, 351);
            this.tabAlignment.TabIndex = 12;
            this.tabAlignment.SelectedIndexChanged += new System.EventHandler(this.tabAlignment_SelectedIndexChanged);
            // 
            // tabHorizontal
            // 
            this.tabHorizontal.Controls.Add(this.pbHView);
            this.tabHorizontal.Location = new System.Drawing.Point(4, 22);
            this.tabHorizontal.Name = "tabHorizontal";
            this.tabHorizontal.Padding = new System.Windows.Forms.Padding(3);
            this.tabHorizontal.Size = new System.Drawing.Size(822, 325);
            this.tabHorizontal.TabIndex = 0;
            this.tabHorizontal.Text = "Horizontal View";
            this.tabHorizontal.UseVisualStyleBackColor = true;
            // 
            // pbHView
            // 
            this.pbHView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pbHView.BackColor = System.Drawing.Color.Gainsboro;
            this.pbHView.Location = new System.Drawing.Point(-11, 19);
            this.pbHView.Margin = new System.Windows.Forms.Padding(2);
            this.pbHView.Name = "pbHView";
            this.pbHView.Size = new System.Drawing.Size(834, 319);
            this.pbHView.TabIndex = 0;
            this.pbHView.TabStop = false;
            this.pbHView.SizeChanged += new System.EventHandler(this.pbHView_SizeChanged);
            // 
            // tabVertical
            // 
            this.tabVertical.Controls.Add(this.pbVView);
            this.tabVertical.Location = new System.Drawing.Point(4, 22);
            this.tabVertical.Name = "tabVertical";
            this.tabVertical.Padding = new System.Windows.Forms.Padding(3);
            this.tabVertical.Size = new System.Drawing.Size(819, 325);
            this.tabVertical.TabIndex = 1;
            this.tabVertical.Text = "Vertical View";
            this.tabVertical.UseVisualStyleBackColor = true;
            // 
            // pbVView
            // 
            this.pbVView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbVView.Location = new System.Drawing.Point(6, 6);
            this.pbVView.Name = "pbVView";
            this.pbVView.Size = new System.Drawing.Size(809, 274);
            this.pbVView.TabIndex = 0;
            this.pbVView.TabStop = false;
            // 
            // tab3D
            // 
            this.tab3D.Controls.Add(this.pb3DView);
            this.tab3D.Location = new System.Drawing.Point(4, 22);
            this.tab3D.Name = "tab3D";
            this.tab3D.Padding = new System.Windows.Forms.Padding(3);
            this.tab3D.Size = new System.Drawing.Size(819, 325);
            this.tab3D.TabIndex = 2;
            this.tab3D.Text = "3D View";
            this.tab3D.UseVisualStyleBackColor = true;
            // 
            // pb3DView
            // 
            this.pb3DView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb3DView.Location = new System.Drawing.Point(3, 3);
            this.pb3DView.Name = "pb3DView";
            this.pb3DView.Size = new System.Drawing.Size(816, 285);
            this.pb3DView.TabIndex = 0;
            this.pb3DView.TabStop = false;
            // 
            // lMateraial
            // 
            this.lMateraial.AutoSize = true;
            this.lMateraial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMateraial.ForeColor = System.Drawing.Color.DarkRed;
            this.lMateraial.Location = new System.Drawing.Point(25, 9);
            this.lMateraial.Name = "lMateraial";
            this.lMateraial.Size = new System.Drawing.Size(145, 15);
            this.lMateraial.TabIndex = 39;
            this.lMateraial.Text = "Alignment Properties:";
            // 
            // frmAlignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 730);
            this.Controls.Add(this.lMateraial);
            this.Controls.Add(this.tabAlignment);
            this.Controls.Add(this.dgvVerticalSegments);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDeleteRow);
            this.Controls.Add(this.btnReplicate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.dgvStartXYZ);
            this.Controls.Add(this.dgvHoizontalSegments);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmAlignment";
            this.Text = "frmAlignment";
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoizontalSegments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStartXYZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVerticalSegments)).EndInit();
            this.tabAlignment.ResumeLayout(false);
            this.tabHorizontal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbHView)).EndInit();
            this.tabVertical.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbVView)).EndInit();
            this.tab3D.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb3DView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvHoizontalSegments;
        private System.Windows.Forms.DataGridView dgvStartXYZ;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReplicate;
        private System.Windows.Forms.Button btnDeleteRow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvVerticalSegments;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TabControl tabAlignment;
        private System.Windows.Forms.TabPage tabHorizontal;
        private System.Windows.Forms.PictureBox pbHView;
        private System.Windows.Forms.TabPage tabVertical;
        private System.Windows.Forms.TabPage tab3D;
        private System.Windows.Forms.DataGridViewTextBoxColumn Start_X;
        private System.Windows.Forms.DataGridViewTextBoxColumn Start_Y;
        private System.Windows.Forms.DataGridViewTextBoxColumn Start_Z;
        private System.Windows.Forms.DataGridViewTextBoxColumn Station;
        private System.Windows.Forms.DataGridViewTextBoxColumn AzimuthStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlopeStart;
        private System.Windows.Forms.PictureBox pbVView;
        private System.Windows.Forms.PictureBox pb3DView;
        private System.Windows.Forms.DataGridViewComboBoxColumn HDirectionCombo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Radius;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lenght;
        private System.Windows.Forms.DataGridViewComboBoxColumn VDirectionCombo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rise;
        private System.Windows.Forms.DataGridViewTextBoxColumn Run;
        private System.Windows.Forms.Label lMateraial;
    }
}