﻿namespace CBridge
{
    partial class frmColumn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bReplicate = new System.Windows.Forms.Button();
            this.lColumn = new System.Windows.Forms.Label();
            this.dgvColumn = new System.Windows.Forms.DataGridView();
            this.InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Base = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Top = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Section = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Variable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColumn)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "lb, in",
            "lb, ft",
            "kip, in",
            "kip, ft"});
            this.cmbUnit.Location = new System.Drawing.Point(613, 382);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(70, 23);
            this.cmbUnit.TabIndex = 41;
            this.cmbUnit.Text = "kip, in";
            this.cmbUnit.SelectedIndexChanged += new System.EventHandler(this.cmbUnit_SelectedIndexChanged_1);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.bCancel.Location = new System.Drawing.Point(775, 378);
            this.bCancel.Margin = new System.Windows.Forms.Padding(2);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(73, 28);
            this.bCancel.TabIndex = 40;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOK.ForeColor = System.Drawing.Color.DarkRed;
            this.bOK.Location = new System.Drawing.Point(698, 378);
            this.bOK.Margin = new System.Windows.Forms.Padding(2);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(73, 28);
            this.bOK.TabIndex = 39;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDelete.ForeColor = System.Drawing.Color.DarkRed;
            this.bDelete.Location = new System.Drawing.Point(125, 378);
            this.bDelete.Margin = new System.Windows.Forms.Padding(2);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(73, 28);
            this.bDelete.TabIndex = 38;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bReplicate
            // 
            this.bReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReplicate.ForeColor = System.Drawing.Color.DarkRed;
            this.bReplicate.Location = new System.Drawing.Point(30, 378);
            this.bReplicate.Margin = new System.Windows.Forms.Padding(2);
            this.bReplicate.Name = "bReplicate";
            this.bReplicate.Size = new System.Drawing.Size(73, 28);
            this.bReplicate.TabIndex = 37;
            this.bReplicate.TabStop = false;
            this.bReplicate.Text = "Replicate";
            this.bReplicate.UseVisualStyleBackColor = true;
            this.bReplicate.Click += new System.EventHandler(this.bReplicate_Click);
            // 
            // lColumn
            // 
            this.lColumn.AutoSize = true;
            this.lColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lColumn.ForeColor = System.Drawing.Color.DarkRed;
            this.lColumn.Location = new System.Drawing.Point(12, 9);
            this.lColumn.Name = "lColumn";
            this.lColumn.Size = new System.Drawing.Size(130, 15);
            this.lColumn.TabIndex = 42;
            this.lColumn.Text = "Column Properties:";
            // 
            // dgvColumn
            // 
            this.dgvColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvColumn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColumn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InName,
            this.name,
            this.InUse,
            this.Distance,
            this.Length,
            this.Base,
            this.Top,
            this.Section,
            this.Variable});
            this.dgvColumn.Location = new System.Drawing.Point(15, 27);
            this.dgvColumn.Name = "dgvColumn";
            this.dgvColumn.Size = new System.Drawing.Size(847, 326);
            this.dgvColumn.TabIndex = 43;
            // 
            // InName
            // 
            this.InName.HeaderText = "InName";
            this.InName.Name = "InName";
            this.InName.Visible = false;
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // InUse
            // 
            this.InUse.HeaderText = "InUse";
            this.InUse.Name = "InUse";
            // 
            // Distance
            // 
            this.Distance.HeaderText = "Distance";
            this.Distance.Name = "Distance";
            // 
            // Length
            // 
            this.Length.HeaderText = "Length";
            this.Length.Name = "Length";
            // 
            // Base
            // 
            this.Base.HeaderText = "Base";
            this.Base.Items.AddRange(new object[] {
            "Lenght",
            "Roller",
            "Pin",
            "Pin+Torsion",
            "Fix",
            "Spring"});
            this.Base.Name = "Base";
            this.Base.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Base.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Top
            // 
            this.Top.HeaderText = "Top";
            this.Top.Items.AddRange(new object[] {
            "Lenght",
            "Roller",
            "Pin",
            "Pin+Torsion",
            "Fix",
            "Spring"});
            this.Top.Name = "Top";
            this.Top.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Top.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Section
            // 
            this.Section.HeaderText = "Section";
            this.Section.Items.AddRange(new object[] {
            "Abc",
            "DEF",
            "Variable"});
            this.Section.Name = "Section";
            this.Section.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Section.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Variable
            // 
            this.Variable.HeaderText = "Variable";
            this.Variable.Name = "Variable";
            // 
            // frmColumn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 427);
            this.Controls.Add(this.dgvColumn);
            this.Controls.Add(this.lColumn);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bReplicate);
            this.Name = "frmColumn";
            this.Text = "frmColumn";
            ((System.ComponentModel.ISupportInitialize)(this.dgvColumn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bReplicate;
        private System.Windows.Forms.Label lColumn;
        private System.Windows.Forms.DataGridView dgvColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Length;
        private System.Windows.Forms.DataGridViewComboBoxColumn Base;
        private System.Windows.Forms.DataGridViewComboBoxColumn Top;
        private System.Windows.Forms.DataGridViewComboBoxColumn Section;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variable;
    }
}