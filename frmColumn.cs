﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZoconFEA3D;                   // Add for anything that needs to access m_Structure or core functions

namespace CBridge
{
    public partial class frmColumn : Form
    {

        bool DoEvents = false;                           // flag for turning event handling off and on selectively

        int m_Unit = 2;                                  // default for cmbUnit index for kip, inch
        string[,] m_HeaderText = new string[4, 5];       // one for each unit name like "ksi, inch"
        double[,] m_UnitFactor = null;                   // unit conversions from ksi to selected unit
        double[,] m_Limits = null;                       //  Min, max, default for each column
        private List<Column> m_ColumnList;
        bool[] m_InUse_Conc = new bool[10];              // Flags for Column being Used , So we cant delete        
        public string[,] m_RenameListConc = null;        // List of Column new names returned with new list
        public bool m_OK = false;

#region userfunctions

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Constructor of Column form
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        public frmColumn()
        {
            InitializeComponent();

            m_Limits = new double[4, 6];        
            
            //  Min, max, default, unit conversion for each column 
            // (units of ksi) {InName, NewName, In-Use, E, nu, alfa, gama)

            // Defaults are set for steel in ksi
            //      min                  max                        default                conversion 
            m_Limits[0, 4] = 0; m_Limits[1, 5] = 1.0e+15; m_Limits[2, 5] = 30; m_Limits[3, 5] = 1;    // Skew 
            m_Limits[0, 5] = 0; m_Limits[1, 5] = 1.0e+15; m_Limits[2, 5] = 30; m_Limits[3, 5] = 1;    // Skew 
    
            // Header Texts 
            string[,] HeaderText =
                                     {{"Kx(psi)", "Ky(pci)", "Kz(psi)", "Krx((psi)" , "Kry((psi)", "Krz((psi)"},
                                      {"Kx(psf)", "Ky(pcf)", "Kz(psf)", "Krx((psf)" , "Kry((psf)", "Krz((psf)"},
                                      {"Kx(ksi)", "Ky(kci)", "Kz(ksi)", "Krx((ksi)" , "Kry((ksi)", "Krz((ksi)"},
                                      {"Kx(ksf)", "Ky(kcf)", "Kz(ksf)", "Krx((ksf)" , "Kry((ksf)", "Krz((ksf)"}};

            m_HeaderText = HeaderText;

            // unit conversion array       
            //                          Skew          kx           ky             kz              krx            kry
            double[,] Unitfactor =   {{1000.0        ,1000.0     ,1000.0        ,1000.0         ,1000.0        ,1000        ,1000      },
                                      {(1000*144)    ,(1000*144) ,(1000*144)    ,(1000*144)     ,(1000*144)    ,(1000*144)  ,(1000*144)},
                                      {1.0           ,1.0        ,1.0           ,1.0            ,1.0           ,1.0         ,1.0       },
                                      {144.0         ,144.0      ,144.0         ,144.0          ,144.0         ,144.0       ,144.0     }};

            m_UnitFactor = Unitfactor;
            cmbUnit.Text = cmbUnit.Items[2].ToString(); // Initialize the dialog to kip, in
            cmbUnit.SelectedIndex = 2;
            m_Unit = 2;
            DoEvents = true; // a flag for turning event handling off and on selectively
        }

        // --------------------------------------------------------------------
        // User Functions
        // --------------------------------------------------------------------

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Grid view
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SetValues(Column[] iColumns)
        {
            if (iColumns != null)
            {
                UpdateGridViews(iColumns);

                bool OK = true;     // = CheckAllLimits();  ///todo

                if (!OK) bOK.Enabled = false;
            }
        }

        /// <summary>
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private bool CheckAllLimits()
        {
            DataGridView dgv = null;

            bool AllOK = true;

            // for checkeing names we need a list of all materila names
            int totalNames = dgvColumn.RowCount - 1;

            string[] ColumnNames = new string[totalNames];

            int count = 0;

            dgv = dgvColumn;

            for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
            {
                ColumnNames[count] = Convert.ToString(dgv.Rows[iR].Cells[1].Value);
                count++;
            }

            string Message = "";

            for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
            {
                for (int iC = 0; iC < dgv.ColumnCount - 1; iC++)
                {
                    string Cellvalue = Convert.ToString(dgv.Rows[iR].Cells[iC].Value);

                    if (iC == 1)
                    {
                        // this is Name column, check against all names in all grids
                    }
                    else if (iC != 2) // do not check the In-Use Column
                    {
                        if (!CheckLimits(dgv, iR, iC, Cellvalue, ref Message))
                        {
                            dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                            dgv.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                            AllOK = false;
                        }
                        else
                        {
                            dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                            dgv.Rows[iR].Cells[iC].Style.BackColor = Color.White;
                        }
                    }
                }
            }

            return AllOK;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Column Grid Views
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void UpdateGridViews(Column[] iColumn)
        {
            DoEvents = false;

            if (iColumn != null && iColumn.Length > 0)
            {
                foreach (Column item in iColumn)
                {
                    UpdateColumnRow((Column)item);
                }
            }

            DoEvents = true;
        }
        
        void UpdateColumnRow(Column item)
        {
            // Show Columns        
            int i = 0;

            // Add a Row

            i = dgvColumn.Rows.Add();
                    
            dgvColumn["InName", i].Value = item.Name;
            dgvColumn["name", i].Value = item.Name; 
            dgvColumn["InUse", i].Value = "No";

            dgvColumn["Distance", i].Value = item.Distance;

            dgvColumn["Length", i].Value = item.Length;
            dgvColumn["Base", i].Value = item.Base;
            dgvColumn["Top", i].Value = item.Top;
            dgvColumn["Section", i].Value = item.Section;
            dgvColumn["Variable", i].Value = item.Variable;
            
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves Grid view data into structure parameters
        ///  
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SaveColumns()
        {
            DoEvents = false;

            if (dgvColumn.Rows.Count > 1)
            {
                SaveColumn();
            }

            DoEvents = true;
        }
        
        void SaveColumn()
        {
            // Make Generate names  
            DataGridView dgv = dgvColumn;

            int count = dgv.Rows.Count - 1;

            m_ColumnList = new List<Column>(count);

            int NumRename = 0;
            double Factor = 1;

            string ColumnName = "";
            double Distance = 0;  
            double Length = 0;    
            string Base = "";      
            string Top=  "";       
            string Section = "";   
            string Variable = "";  

          
            for (int i = 0; i < count; i++)
            {
                if (dgv[0, i].Value != null) ColumnName = dgv[0, i].Value.ToString();

                if (ColumnName == "") ColumnName = dgv[1, i].Value.ToString();

                Distance = Convert.ToInt32(dgv[3, i].Value);
                Length = Convert.ToInt32(dgv[4, i].Value); 

                Base = dgv[5, i].Value.ToString();
                Top  = dgv[6, i].Value.ToString();

                Section = dgv[7, i].Value.ToString();
                Variable  = dgv[8, i].Value.ToString();

                Column Column1 = new Column(ColumnName, Distance, Length, 0, Base, Top, Section);

                Column1.Name = ColumnName;
                m_ColumnList.Add(Column1);
                
                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv["InName", i].Value != null)
                {
                    if (dgv["InName", i].Value.ToString() != dgv["name", i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use Column
                dgv["InUse", i].Value = "NO";
            }

            // prepare list of renamed items
            m_RenameListConc = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < count; i++)
            {
                if (dgv["InName", i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv["InName", i].Value.ToString() != dgv["Name", i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListConc[iRename, 0] = dgv["InName", i].Value.ToString();
                        m_RenameListConc[iRename, 1] = dgv["Name", i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }

      
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Replicat Rows of Data Grid  
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void ReplicateRow(DataGridView dgv, int RIndex)
        {
            bool EventStatus = DoEvents;

            DoEvents = false;

            int iRow = dgv.Rows.Add(); // The newly added row index is the last row

            string baseName = "New_0";

            string[] NameList = new string[dgv.Rows.Count - 2];

            for (int i = 0; i < dgv.Rows.Count - 2; i++)
                NameList[i] = dgv[1, i].Value.ToString();

            string newName = GetName(baseName, NameList);

            dgv[0, iRow].Value = newName;
            dgv[1, iRow].Value = newName;

            dgv[2, iRow].Value = "No";

            for (int j = 3; j < dgv.Columns.Count; j++)
            {
                dgv[j, iRow].Value = dgv[j, RIndex].Value;
            }

            DoEvents = EventStatus;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Get Name 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private string GetName(string BaseName, string[] NameList)
        {
        IncrementName:

            string NewName = BaseName.Substring(0, BaseName.Length - 1);
            string right = BaseName.Substring(BaseName.Length - 1); // rightmost charachter

            try
            {
                int n = Convert.ToInt16(right);

                if (n == 9) NewName = NewName + "10";

                else NewName = NewName + Convert.ToString(n + 1);
            }
            catch
            {
                NewName = BaseName + "0";
            }

            for (int i = 0; i < NameList.Length; i++) // Check for duplicate names
            {
                if (NameList[i] == NewName)
                {
                    BaseName = NewName;

                    goto IncrementName;
                }
            }
            return NewName;
        }

#endregion userfunctions

#region eventfunctions

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Event Functions 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Replicating Rows 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void dgvColumn_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DoEvents)
            {
                DoEvents = false;

                DataGridView dgv = dgvColumn;
                int i = dgv.CurrentRow.Index;

                string baseName = "New";
                string[] NameList = new string[dgv.Rows.Count - 2];

                for (int j = 0; j < dgv.Rows.Count - 2; j++) NameList[j] = dgv[1, j].Value.ToString();
                string newName = GetName(baseName, NameList);

                dgv[1, i].Value = newName;
                dgv[2, i].Value = "No";
                dgv[3, i].Value = "Seat";
                dgv[4, i].Value = "Spring";

                if (i == 0)
                {
                    for (int j = 5; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, 0].Value = m_Limits[2, j];  // use default values from m_Limits column 2
                    }
                }
                else
                {
                    for (int j = 5; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, i].Value = dgv[j, i - 1].Value;
                    }
                }

                DoEvents = true;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Button delete and buttomn OKs 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void bDelete_Click(object sender, EventArgs e)
        {
            DataGridView dgv = dgvColumn;

            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgv.SelectedRows)
                {
                    if (item.Index != dgv.Rows.Count - 1)
                    {
                        if (item.Cells[2].Value.ToString() == "No") dgv.Rows.Remove(item);
                    }
                }
            }

            CheckAllLimits();
        }

        private void bReplicate_Click(object sender, EventArgs e)
        {

            DataGridView dgv = dgvColumn;

            foreach (DataGridViewRow item in dgv.SelectedRows)
            {
                if (item.Index != dgv.Rows.Count - 1) ReplicateRow(dgv, item.Index);
            }
        }


        private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!DoEvents) return;

            DoEvents = false;

            // Convert Concrete values
            dgvColumn.Columns[3].DefaultCellStyle.Format = "#.###";

            int SelectedUnit = cmbUnit.SelectedIndex;

            ConvertCellUnits(dgvColumn, SelectedUnit);


            m_Unit = SelectedUnit;

            DoEvents = true;
        }

        void ConvertCellUnits(DataGridView dgv, int SelectedUnit)
        {

            string GridName = dgv.Name;
            string[,] HeaderText = null;

            HeaderText = m_HeaderText;

            int LastColumn = dgv.ColumnCount;

            for (int i = 3; i < LastColumn; i++)
            {
                dgv.Columns[i].HeaderText = HeaderText[SelectedUnit, i - 3];
            }

            for (int j = 3; j < LastColumn; j++)
            {
                int jj = j - 3;

                double Factor = m_UnitFactor[SelectedUnit, jj] / m_UnitFactor[m_Unit, jj];

                m_Limits[3, j] = m_Limits[3, j] * Factor;

                for (int ir = 0; ir < dgv.Rows.Count - 1; ir++)
                {
                    dgv[j, ir].Value = (Convert.ToDouble(dgv[j, ir].Value)) * Factor;
                }
            }
        }



        private void bCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // Reset the Column List
            SaveColumns();

            //Close;
            m_OK = true;
            this.Close();
            return;
        }

        public void UpdateBridgeInfo(ref Column[] iColumn)
        {
            int num = 0;
            if (m_ColumnList != null) num += m_ColumnList.Count;

            if (num > 0)
            {
                iColumn = new Column[num];

                int i = 0;

                if (m_ColumnList != null)
                {
                    foreach (Column item in m_ColumnList)
                    {
                        iColumn[i++] = item;
                    }
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  When any Cell value change, we check all cell to make sure the are withing the set limits
        /// 
        /// 
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        private void dgvColumn_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvColumn.CurrentRow == null) return;

                string Message = "";

                int Row = dgvColumn.CurrentRow.Index;

                int Col = dgvColumn.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvColumn.CurrentCell.Value);

                if (!CheckLimits(dgvColumn, Row, Col, Cellvalue, ref Message))
                {
                    dgvColumn.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvColumn.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    bOK.Enabled = true; //CheckAllLimits(); // Temporary; We do not need the check after this

                }
            }
        }


        private bool CheckLimits(DataGridView dgv, int Row, int Column, string Value, ref string Message)
        {
            bool RetCode = true;
            try
            {
                int caseCode = 0; // We never need to check InName

                if (Column == 1) caseCode = 1; // Name

                if (Column == 2)
                {
                    System.Diagnostics.Debugger.Break();
                }

                if (Column == 4)
                {
                    if (Value != "Spring")
                    {
                        // disable and gray out all kx and krx cells in this row
                        for (int i = 6; i < 12; i++)
                        {
                            dgvColumn.Rows[Row].Cells[i].Value = "";
                            dgvColumn.Rows[Row].Cells[i].Style.BackColor = Color.LightGray;
                            dgvColumn.Rows[Row].Cells[i].ReadOnly = true;
                        }
                    }
                    else
                    {
                        // enable all kx and krx cells in this row
                        for (int i = 6; i < 12; i++)
                        {
                            dgvColumn.Rows[Row].Cells[i].Style.BackColor = Color.White;
                            dgvColumn.Rows[Row].Cells[i].ReadOnly = false;
                        }
                    }
                }

                if (Column > 4) caseCode = 2; // Col 3 and 4 are drop downs and dont need to check

                switch (caseCode)
                {
                    case 1:  // Check for duplicate names
                        for (int i = 0; i < dgv.Rows.Count - 1; i++)
                        {
                            string CheckName = dgv[1, i].Value.ToString();

                            if (Value == CheckName && i != Row)
                            {
                                RetCode = false;
                                Message = "Duplicate Name";
                                break;
                            }
                        }
                        break;

                    case 2:  // Check Limits

                        double dValue = Convert.ToDouble(Value);

                        if (dValue < m_Limits[0, Column] || dValue > m_Limits[1, Column])
                        {
                            RetCode = false;
                            Message = "Out of Range";
                            break;
                        }
                        break;

                    default:

                        break;
                }
            }
            catch
            {
                RetCode = false;
                Message = "Limit Check Exception";
            }
            return RetCode;
        }

        private void dgvColumn_SelectionChanged(object sender, EventArgs e)
        {

            if (DoEvents)
            {
                DoEvents = false;

                if (dgvColumn.SelectedRows.Count > 0)
                {
                    bDelete.Enabled = true;
                    bReplicate.Enabled = true;
                }
                else
                {
                    bDelete.Enabled = false;
                    bReplicate.Enabled = false;
                }

                DoEvents = true;
            }
        }

#endregion eventfunctions


        private void cmbUnit_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
    }
}
