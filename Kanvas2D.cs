﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace CBridge
{
    public class Kanvas2D
    {
        protected Pen[] m_Pens; // List of pens for drawing
        protected Brush[] m_Brushes; // List of brushes for drawing
        public enum K2DSymbols { Star, Triangle, Hexagon, Burst, Octagon };
        protected double[,] m_Points; // [i,5]: X1,Y1 (actual coordinates), x1,y1(graphgic window coordinates, i.e., x1=X-XofLeftofViewport), pen#
        protected double[,] m_Lines; // [i,9]: X1,Y1,X2,Y2, x1,y1,x2,y2, pen#
        protected double[][] m_PolyLines; // [iset][4*npt+1]:  X1,Y1, x1,y1 for each point, pen#
        protected double[][] m_Polygons; // [iset][4*npt+2]:  X1,Y1, x1,y1 for each point, pen#, brush#
        protected double[,] m_SymPoints; // [i,5]: X1,Y1, x1,y1, Symbol#
        protected int[][,] m_Symbols; // [i][npt,2]: points of symbol, pen#, sizeFactor
        protected double m_Zoomfactor; // limits of screen * -0.5 to 0.5
        protected double[][][] m_Limits; // [2][2][2]: [2]=0=global;1=viewport;[2]=x,y;[2]=Origin, length


        public Kanvas2D()
        {
            m_Pens = new Pen[1];
            m_Pens[0] = new Pen(Color.Black, 2);
            m_Brushes = new Brush[1];
            m_Brushes[0] = new SolidBrush(Color.FromArgb(0, Color.Black)); // Fully transparent black=no fill
            m_Points = new double[0, 5];
            m_Lines = new double[0, 9];
            m_PolyLines = new double[0][];
            m_Polygons = new double[0][];
            m_SymPoints = new double[0, 5];
            m_Symbols = new int[0][,];
            m_Zoomfactor = 1.0;
            m_Limits = new double[2][][];

            for (int i = 0; i < 2; i++) { m_Limits[i] = new double[2][]; for (int j = 0; j < 2; j++) m_Limits[i][j] = new double[2]; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 2; k++) m_Limits[i][k][0] = 0; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 2; k++) m_Limits[i][k][1] = 1; }
        }

        public int AddPen(Pen Pen1)
        {
            int NPen = m_Pens.Length;
            Pen[] Pen2 = new Pen[NPen + 1];
            for (int i = 0; i < NPen; i++)
            {
                Pen2[i] = m_Pens[i];
            }
            Pen2[NPen] = Pen1;
            m_Pens = Pen2;
                      
            return NPen;
        }

        public int AddBrush(Brush Brush1)
        {
            int NBrush = m_Brushes.Length;
            Brush[] Brush2 = new Brush[NBrush + 1];
            for (int i = 0; i < NBrush; i++)
            {
                Brush2[i] = m_Brushes[i];
            }
            Brush2[NBrush] = Brush1;
            m_Brushes = Brush2;
            return NBrush;
        }

        public bool AddPoint(double X, double Y, int PenNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                int Npt = m_Points.GetLength(0);
                double[,] Points = new double[Npt + 1, m_Points.GetLength(1)];
                for (int i = 0; i < Npt; i++)
                {
                    for (int j = 0; j < m_Points.GetLength(1); j++)
                    {
                        Points[i, j] = m_Points[i, j];
                    }
                }
                Points[Npt, 0] = X;
                Points[Npt, 1] = Y;
                Points[Npt, 4] = PenNum;
                m_Points = Points;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public bool AddSymPoint(double X, double Y, int SymNum)
        {
            bool RetCode = false;
            try
            {
                if (SymNum >= m_Symbols.Length || SymNum < 0) SymNum = 0;
                int Npt = m_SymPoints.GetLength(0);
                double[,] Points = new double[Npt + 1, m_SymPoints.GetLength(1)];
                for (int i = 0; i < Npt; i++)
                {
                    for (int j = 0; j < m_SymPoints.GetLength(1); j++)
                    {
                        Points[i, j] = m_SymPoints[i, j];
                    }
                }
                Points[Npt, 0] = X;
                Points[Npt, 1] = Y;
                Points[Npt, 4] = SymNum;
                m_SymPoints = Points;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public bool AddLine(double X1, double Y1, double X2, double Y2, int PenNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                int NLn = m_Lines.GetLength(0);
                double[,] Lines = new double[NLn + 1, m_Lines.GetLength(1)];
                for (int i = 0; i < NLn; i++)
                {
                    for (int j = 0; j < m_Lines.GetLength(1); j++)
                    {
                        Lines[i, j] = m_Lines[i, j];
                    }
                }
                Lines[NLn, 0] = X1;
                Lines[NLn, 1] = Y1;
                Lines[NLn, 2] = X2;
                Lines[NLn, 3] = Y2;
                Lines[NLn, 8] = PenNum;
                m_Lines = Lines;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        /// <summary>
        /// Save a polyline for drawing
        /// </summary>
        /// <param name="XYs">[npt,2]X&Y of points of polyline</param>
        /// <param name="PenNum">reference to a pen number for the lines, invalid pen->use pen #0</param>
        /// <returns>true=Ok, false=failed</returns>
        public bool AddPolyLine(double[,] XYs, int PenNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                int NPL = m_PolyLines.GetLength(0);
                double[][] PLines = new double[NPL + 1][];
                for (int i = 0; i < NPL; i++)
                {
                    PLines[i] = m_PolyLines[i];
                }
                int nPt = XYs.GetLength(0); ;
                PLines[NPL] = new double[4 * nPt + 1];
                for (int j = 0; j < nPt; j++)
                {
                    PLines[NPL][4 * j] = XYs[j, 0];
                    PLines[NPL][4 * j + 1] = XYs[j, 1];
                }
                PLines[NPL][4 * nPt] = PenNum;
                m_PolyLines = PLines;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        /// <summary>
        /// Save a polygon for drawing
        /// </summary>
        /// <param name="XYs">[npt,2]X&Y of points of polyline</param>
        /// <param name="PenNum">reference to a pen number for the lines, invalid pen->use pen #0</param>
        /// <param name="BrushNum">reference to a brush number for the fill, invalid brush->use brush #0</param>
        /// <returns>true=Ok, false=failed</returns>
        public bool AddPolygon(double[,] XYs, int PenNum, int BrushNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                if (BrushNum >= m_Brushes.Length || BrushNum < 0) BrushNum = 0;
                int NPG = m_Polygons.GetLength(0);
                double[][] PGs = new double[NPG + 1][];
                for (int i = 0; i < NPG; i++)
                {
                    PGs[i] = m_Polygons[i];
                }
                int nPt = XYs.GetLength(0); ;
                PGs[NPG] = new double[4 * nPt + 2];
                for (int j = 0; j < nPt; j++)
                {
                    PGs[NPG][4 * j] = XYs[j, 0];
                    PGs[NPG][4 * j + 1] = XYs[j, 1];
                }
                PGs[NPG][4 * nPt] = PenNum;
                PGs[NPG][4 * nPt + 1] = BrushNum;
                m_Polygons = PGs;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public void SetWinSize(int WinX, int WinY)
        {
            m_Limits[1][0][1] = WinX;
            m_Limits[1][1][1] = WinY;
            SetZoomFactor();
            ResetPicture();
        }
        public int[] WinSize()
        {
            int[] XY = new int[2];
            XY[0] = Convert.ToInt32(m_Limits[1][0][1]);
            XY[1] = Convert.ToInt32(m_Limits[1][1][1]);
            return XY;
        }
        public void SetView(double Xmin, double Ymin, double Xmax, double Ymax)
        {
            double XL = Xmax - Xmin;
            double YL = Ymax - Ymin;
            m_Limits[0][0][0] = Xmin + XL / 2.0;
            m_Limits[0][1][0] = Ymin + YL / 2.0;
            m_Limits[0][0][1] = XL;
            m_Limits[0][1][1] = YL;
            SetZoomFactor();
        }
        public void SetZoomFactor()
        {
            double XL = m_Limits[0][0][1];
            double YL = m_Limits[0][1][1];
            double WinX = m_Limits[1][0][1];
            double WinY = m_Limits[1][1][1];

            double XFac = WinX / XL;
            double YFac = WinY / YL;
            m_Zoomfactor = Math.Min(XFac, YFac);
        }
        public void ZoomFit()
        {
            double Xmin, Xmax, Ymin, Ymax;
            int NPT = m_Points.GetLength(0);
            int NSPT = m_SymPoints.GetLength(0);
            int NLine = m_Lines.GetLength(0);
            int NPL = m_PolyLines.GetLength(0);
            int NPG = m_Polygons.GetLength(0);
            if (NPT > 0)
            {
                Xmin = m_Points[0, 0];
                Xmax = m_Points[0, 0];
                Ymin = m_Points[0, 1];
                Ymax = m_Points[0, 1];
            }
            else if (NSPT > 0)
            {
                Xmin = m_SymPoints[0, 0];
                Xmax = m_SymPoints[0, 0];
                Ymin = m_SymPoints[0, 1];
                Ymax = m_SymPoints[0, 1];
            }
            else if (NLine > 0)
            {
                Xmin = m_Lines[0, 0];
                Xmax = m_Lines[0, 0];
                Ymin = m_Lines[0, 1];
                Ymax = m_Lines[0, 1];
            }
            else if (NPL > 0)
            {
                int npt1 = (m_PolyLines[0].Length - 1) / 4 - 1;
                Xmin = m_PolyLines[0][0];
                Xmax = m_PolyLines[0][1];
                Ymin = m_PolyLines[0][4 * npt1];
                Ymax = m_PolyLines[0][4 * npt1 + 1];
            }
            else if (NPG > 0)
            {
                int npt1 = (m_Polygons[0].Length - 2) / 4 - 1;
                Xmin = m_Polygons[0][0];
                Xmax = m_Polygons[0][1];
                Ymin = m_Polygons[0][4 * npt1];
                Ymax = m_Polygons[0][4 * npt1 + 1];
            }
            else
            {
                return;
            }
            for (int i = 0; i < NPT; i++)
            {
                Xmin = Math.Min(Xmin, m_Points[i, 0]);
                Xmax = Math.Max(Xmax, m_Points[i, 0]);
                Ymin = Math.Min(Ymin, m_Points[i, 1]);
                Ymax = Math.Max(Ymax, m_Points[i, 1]);
            }
            for (int i = 0; i < NSPT; i++)
            {
                Xmin = Math.Min(Xmin, m_SymPoints[i, 0]);
                Xmax = Math.Max(Xmax, m_SymPoints[i, 0]);
                Ymin = Math.Min(Ymin, m_SymPoints[i, 1]);
                Ymax = Math.Max(Ymax, m_SymPoints[i, 1]);
            }
            for (int i = 0; i < NLine; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Xmin = Math.Min(Xmin, m_Lines[i, 0 + 2 * j]);
                    Xmax = Math.Max(Xmax, m_Lines[i, 0 + 2 * j]);
                    Ymin = Math.Min(Ymin, m_Lines[i, 1 + 2 * j]);
                    Ymax = Math.Max(Ymax, m_Lines[i, 1 + 2 * j]);
                }
            }
            for (int i = 0; i < NPL; i++)
            {
                int npt = (m_PolyLines[0].Length - 1) / 4;
                for (int j = 0; j < npt; j++)
                {
                    Xmin = Math.Min(Xmin, m_PolyLines[i][4 * j]);
                    Xmax = Math.Max(Xmax, m_PolyLines[i][4 * j]);
                    Ymin = Math.Min(Ymin, m_PolyLines[i][4 * j + 1]);
                    Ymax = Math.Max(Ymax, m_PolyLines[i][4 * j + 1]);
                }
            }
            for (int i = 0; i < NPG; i++)
            {
                int npt = (m_Polygons[0].Length - 2) / 4;
                for (int j = 0; j < npt; j++)
                {
                    Xmin = Math.Min(Xmin, m_Polygons[i][4 * j]);
                    Xmax = Math.Max(Xmax, m_Polygons[i][4 * j]);
                    Ymin = Math.Min(Ymin, m_Polygons[i][4 * j + 1]);
                    Ymax = Math.Max(Ymax, m_Polygons[i][4 * j + 1]);
                }
            }
            double XL = 1.1 * (Xmax - Xmin); // add 10% margin
            double YL = 1.1 * (Ymax - Ymin);
            double Xo = 0.5 * (Xmin + Xmax);
            double Yo = 0.5 * (Ymin + Ymax);
            double Ltol = 1.0e-10;
            if (XL <= 0) XL = Ltol;
            if (YL <= 0) YL = Ltol;
            m_Limits[0][0][0] = Xo;
            m_Limits[0][1][0] = Yo;
            m_Limits[0][0][1] = XL;
            m_Limits[0][1][1] = YL;
            ResetPicture();
        }
        public void Zoom(double ratio)
        {
            m_Limits[0][0][1] *= ratio;
            m_Limits[0][1][1] *= ratio;
            ResetPicture();
        }
        public void pan(double dX, double dY, int UpdateFlag = 1)
        {
            m_Limits[0][0][0] += dX;  // Xo
            m_Limits[0][1][0] += dY;  // Yo
            ResetPicture();
            if (UpdateFlag == 0) { m_Limits[0][0][0] -= dX; m_Limits[0][1][0] -= dY; } // if no update -> reset the origin where it was
        }
        public void ResetPicture()
        {
            SetZoomFactor();
            for (int i = 0; i < m_Points.GetLength(0); i++)
            {
                double xleft = m_Points[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_Points[i, 1] - m_Limits[0][1][0]);
                m_Points[i, 2] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Points[i, 3] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
            for (int i = 0; i < m_SymPoints.GetLength(0); i++)
            {
                double xleft = m_SymPoints[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_SymPoints[i, 1] - m_Limits[0][1][0]);
                m_SymPoints[i, 2] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_SymPoints[i, 3] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                double xleft = m_Lines[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_Lines[i, 1] - m_Limits[0][1][0]);
                m_Lines[i, 4] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Lines[i, 5] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
                xleft = m_Lines[i, 2] - m_Limits[0][0][0];
                ytop = -(m_Lines[i, 3] - m_Limits[0][1][0]);
                m_Lines[i, 6] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Lines[i, 7] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
            for (int i = 0; i < m_PolyLines.GetLength(0); i++)
            {
                int npt = (m_PolyLines[0].Length - 1) / 4;
                for (int j = 0; j < npt; j++)
                {
                    double[] xy = ResetPoint(m_PolyLines[i][4 * j], m_PolyLines[i][4 * j + 1]);
                    m_PolyLines[i][4 * j + 2] = xy[0];
                    m_PolyLines[i][4 * j + 3] = xy[1];
                }
            }
            for (int i = 0; i < m_Polygons.GetLength(0); i++)
            {
                int npt = (m_Polygons[0].Length - 2) / 4;
                for (int j = 0; j < npt; j++)
                {
                    double[] xy = ResetPoint(m_Polygons[i][4 * j], m_Polygons[i][4 * j + 1]);
                    m_Polygons[i][4 * j + 2] = xy[0];
                    m_Polygons[i][4 * j + 3] = xy[1];
                }
            }
        }
        /// <summary>
        /// Given actual X&Y, returns the viewport x,y
        /// </summary>
        /// <param name="X">Actual X </param>
        /// <param name="Y">Actual Y</param>
        /// <returns>[2] x,y</returns>
        private double[] ResetPoint(double X, double Y)
        {
            double[] XY = new double[2];
            double xleft = X - m_Limits[0][0][0];
            double ytop = -(Y - m_Limits[0][1][0]);
            XY[0] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
            XY[1] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            return XY;
        }
        public Bitmap GetPicture()
        {
            Bitmap DrawArea = new Bitmap(WinSize()[0], WinSize()[1]);
            Graphics g = Graphics.FromImage(DrawArea);
            GetGraphic(ref g);
            return DrawArea;
        }
        public void GetGraphic(ref Graphics g)
        {
            for (int i = 0; i < m_Points.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Points[i, 4]);
                int X = Convert.ToInt16(m_Points[i, 2]);
                int Y = Convert.ToInt16(m_Points[i, 3]);
                g.DrawRectangle(m_Pens[PenNum], X - 1, Y - 1, 2, 2);
            }
            for (int i = 0; i < m_SymPoints.GetLength(0); i++)
            {
                int SymNum = Convert.ToInt16(m_SymPoints[i, 4]);
                int[,] Symbol = m_Symbols[SymNum];
                int PenNum = Symbol[0, 0];
                int Scale = Symbol[0, 1];
                int NPt = Symbol.GetLength(0);
                int Xs = Convert.ToInt16(m_SymPoints[i, 2]);
                int Ys = Convert.ToInt16(m_SymPoints[i, 3]);
                int X1 = Xs + Scale * Symbol[NPt - 1, 0];
                int Y1 = Ys - Scale * Symbol[NPt - 1, 1];
                for (int p = 1; p < NPt; p++)
                {
                    int X2 = Xs + Scale * Symbol[p, 0];
                    int Y2 = Ys - Scale * Symbol[p, 1];
                    g.DrawLine(m_Pens[PenNum], X1, Y1, X2, Y2);
                    X1 = X2;
                    Y1 = Y2;
                }
            }
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines[i, 8]);
                int[] xPt = new int[2];
                int[] yPt = new int[2];
                for (int j = 0; j < 2; j++)
                {
                    xPt[j] = Convert.ToInt16(m_Lines[i, 2 * j + 4]);
                    yPt[j] = Convert.ToInt16(m_Lines[i, 2 * j + 5]);
                }
                g.DrawLine(m_Pens[PenNum], xPt[0], yPt[0], xPt[1], yPt[1]);
            }
            for (int i = 0; i < m_PolyLines.GetLength(0); i++)
            {
                int npt = (m_PolyLines[0].Length - 1) / 4;
                int PenNum = Convert.ToInt16(m_PolyLines[i][4 * npt]);
                int x0 = Convert.ToInt16(m_PolyLines[i][2]);
                int y0 = Convert.ToInt16(m_PolyLines[i][3]);
                for (int j = 1; j < npt; j++)
                {
                    int x1 = Convert.ToInt16(m_PolyLines[i][4 * j + 2]);
                    int y1 = Convert.ToInt16(m_PolyLines[i][4 * j + 3]);
                    g.DrawLine(m_Pens[PenNum], x0, y0, x1, y1);
                    x0 = x1; y0 = y1;
                }
            }
            for (int i = 0; i < m_Polygons.GetLength(0); i++)
            {
                int npt = (m_Polygons[0].Length - 2) / 4;
                Point[] PGpoints = new Point[npt];
                int PenNum = Convert.ToInt16(m_Polygons[i][4 * npt]);
                int BrushNum = Convert.ToInt16(m_Polygons[i][4 * npt + 1]);
                for (int j = 0; j < npt; j++)
                {
                    int x = Convert.ToInt16(m_Polygons[i][4 * j + 2]);
                    int y = Convert.ToInt16(m_Polygons[i][4 * j + 3]);
                    PGpoints[j] = new Point(x, y);
                }
                g.DrawPolygon(m_Pens[PenNum], PGpoints);
                g.FillPolygon(m_Brushes[BrushNum], PGpoints);
            }
        }
        public void AddSymbol(K2DSymbols SymName, int PenNum, int ScaleFactor)
        {
            int[,] SymPoints = null;
            switch (SymName)
            {
                case K2DSymbols.Star:
                    SymPoints = new int[6, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 2;
                    SymPoints[3, 0] = -1;
                    SymPoints[3, 1] = -2;
                    SymPoints[4, 0] = 2;
                    SymPoints[4, 1] = 1;
                    SymPoints[5, 0] = -2;
                    SymPoints[5, 1] = 1;
                    break;
                case K2DSymbols.Triangle:
                    SymPoints = new int[4, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 2;
                    SymPoints[1, 1] = -1;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 2;
                    SymPoints[3, 0] = -2;
                    SymPoints[3, 1] = -1;
                    break;
                case K2DSymbols.Hexagon:
                    SymPoints = new int[7, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 2;
                    SymPoints[2, 1] = 0;
                    SymPoints[3, 0] = 1;
                    SymPoints[3, 1] = 2;
                    SymPoints[4, 0] = -1;
                    SymPoints[4, 1] = 2;
                    SymPoints[5, 0] = -2;
                    SymPoints[5, 1] = 0;
                    SymPoints[6, 0] = -1;
                    SymPoints[6, 1] = -2;
                    break;
                case K2DSymbols.Burst:
                    SymPoints = new int[15, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 0;
                    SymPoints[1, 1] = 0;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 1;
                    SymPoints[3, 0] = 0;
                    SymPoints[3, 1] = 0;
                    SymPoints[4, 0] = -1;
                    SymPoints[4, 1] = 1;
                    SymPoints[5, 0] = 0;
                    SymPoints[5, 1] = 0;
                    SymPoints[6, 0] = -1;
                    SymPoints[6, 1] = 0;
                    SymPoints[7, 0] = 0;
                    SymPoints[7, 1] = 0;
                    SymPoints[8, 0] = -1;
                    SymPoints[8, 1] = -1;
                    SymPoints[9, 0] = 0;
                    SymPoints[9, 1] = 0;
                    SymPoints[10, 0] = 0;
                    SymPoints[10, 1] = -1;
                    SymPoints[11, 0] = 0;
                    SymPoints[11, 1] = 0;
                    SymPoints[12, 0] = 1;
                    SymPoints[12, 1] = -1;
                    SymPoints[13, 0] = 0;
                    SymPoints[13, 1] = 0;
                    SymPoints[14, 0] = 1;
                    SymPoints[14, 1] = 0;
                    SymPoints[13, 0] = 0;
                    SymPoints[13, 1] = 0;
                    SymPoints[14, 0] = 1;
                    SymPoints[14, 1] = 1;
                    break;
                case K2DSymbols.Octagon:
                    SymPoints = new int[9, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 2;
                    SymPoints[2, 1] = -1;
                    SymPoints[3, 0] = 2;
                    SymPoints[3, 1] = 1;
                    SymPoints[4, 0] = 1;
                    SymPoints[4, 1] = 2;
                    SymPoints[5, 0] = -1;
                    SymPoints[5, 1] = 2;
                    SymPoints[6, 0] = -2;
                    SymPoints[6, 1] = 1;
                    SymPoints[7, 0] = -2;
                    SymPoints[7, 1] = -1;
                    SymPoints[8, 0] = -1;
                    SymPoints[8, 1] = -2;
                    break;
            }
            int NSym = m_Symbols.Length;
            int[][,] Symbols = new int[NSym + 1][,];
            for (int i = 0; i < NSym; i++)
            {
                Symbols[i] = m_Symbols[i];
            }
            Symbols[NSym] = SymPoints;
            m_Symbols = Symbols;
        }

        public int[] GetLines(double X, double Y, ref double[] LRatios)
        {
            int[] Lines = new int[0];
            double[] rat = new double[0];
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                bool Online = false;
                int Tolerance = 4;
                double x1 = m_Lines[i, 4];
                double y1 = m_Lines[i, 5];
                double x2 = m_Lines[i, 6];
                double y2 = m_Lines[i, 7];
                double LRatio = -1.0;
                if (x1 == x2)
                {
                    LRatio = (Y - y1) / (y2 - y1);
                    if (Math.Abs(X - x1) < Tolerance && (Y - y1) * (Y - y2) <= 0) Online = true;
                }
                else
                {
                    double m1 = (y2 - y1) / (x2 - x1);
                    double dL = 0;
                    if (m1 == 0)
                    {
                        dL = Math.Abs(Y - y1);
                        LRatio = (X - x1) / (x2 - x1);
                    }
                    else
                    {
                        double m2 = -1 / m1;
                        double y01 = y1 - m1 * x1;
                        double y02 = Y - m2 * X;
                        double xIntersect = -(y02 - y01) / (m2 - m1);
                        double yIntersect = y01 + m1 * xIntersect;
                        dL = Math.Sqrt(Math.Pow((X - xIntersect), 2) + Math.Pow((Y - yIntersect), 2));
                        LRatio = (xIntersect - x1) / (x2 - x1);
                        if ((xIntersect - x1) * (xIntersect - x2) > 0) dL = Tolerance + 1; // if the intersect is not within line don't select
                    }
                    if (dL < Tolerance && LRatio >= 0 && LRatio <= 1) Online = true;
                }
                if (Online)
                {
                    int[] Lines2 = new int[Lines.Length + 1];
                    double[] rat2 = new double[Lines.Length + 1];
                    for (int j = 0; j < Lines.Length; j++)
                    {
                        Lines2[j] = Lines[j];
                        rat2[j] = rat[j];
                    }
                    Lines2[Lines.Length] = i;
                    rat2[Lines.Length] = LRatio;
                    Lines = Lines2;
                    rat = rat2;
                }

            }
            LRatios = rat;
            return Lines;
        }
    }
}




/*

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace CBridge
{
    public class Kanvas2D
    {
        public enum K2DSymbols { Star, Triangle, Hexagon, Burst, Octagon };
        protected Pen[] m_Pens; // List of pens for drawing
        protected double[,] m_Lines; // [i,9]: X1,Y1,X2,Y2, x1,y1,x2,y2, pen#
        protected double[,] m_Points; // [i,5]: X1,Y1, x1,y1, pen#
        protected double[,] m_SymPoints; // [i,5]: X1,Y1, x1,y1, Symbol#
        protected int[][,] m_Symbols; // [i][npt,2]: points of symbol, pen#, sizeFactor
        protected double m_Zoomfactor; // limits of screen * -0.5 to 0.5
        protected double[][][] m_Limits; // [2][2][2]: [2]=0=global;1=viewport;[2]=x,y;[2]=Origin, length


        public Kanvas2D()
        {
            m_Pens = new Pen[1];
            m_Pens[0] = new Pen(Color.Black, 2);
            m_Lines = new double[0, 9];
            m_Points = new double[0, 5];
            m_SymPoints = new double[0, 5];
            m_Symbols = new int[0][,];
            m_Zoomfactor = 1.0;
            m_Limits = new double[2][][];
            for (int i = 0; i < 2; i++) { m_Limits[i] = new double[2][]; for (int j = 0; j < 2; j++) m_Limits[i][j] = new double[2]; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 2; k++) m_Limits[i][k][0] = 0; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 2; k++) m_Limits[i][k][1] = 1; }
        }

        public int AddPen(Pen Pen1)
        {
            int NPen = m_Pens.Length;
            Pen[] Pen2 = new Pen[NPen + 1];
            for (int i = 0; i < NPen; i++)
            {
                Pen2[i] = m_Pens[i];
            }
            Pen2[NPen] = Pen1;
            m_Pens = Pen2;
            return NPen;
        }

        public bool AddPoint(double X, double Y, int PenNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                int Npt = m_Points.GetLength(0);
                double[,] Points = new double[Npt + 1, m_Points.GetLength(1)];
                for (int i = 0; i < Npt; i++)
                {
                    for (int j = 0; j < m_Points.GetLength(1); j++)
                    {
                        Points[i, j] = m_Points[i, j];
                    }
                }
                Points[Npt, 0] = X;
                Points[Npt, 1] = Y;
                Points[Npt, 4] = PenNum;
                m_Points = Points;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public bool AddSymPoint(double X, double Y, int SymNum)
        {
            bool RetCode = false;
            try
            {
                if (SymNum >= m_Symbols.Length || SymNum < 0) SymNum = 0;
                int Npt = m_SymPoints.GetLength(0);
                double[,] Points = new double[Npt + 1, m_SymPoints.GetLength(1)];
                for (int i = 0; i < Npt; i++)
                {
                    for (int j = 0; j < m_SymPoints.GetLength(1); j++)
                    {
                        Points[i, j] = m_SymPoints[i, j];
                    }
                }
                Points[Npt, 0] = X;
                Points[Npt, 1] = Y;
                Points[Npt, 4] = SymNum;
                m_SymPoints = Points;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public bool AddLine(double X1, double Y1, double X2, double Y2, int PenNum)
        {
            bool RetCode = false;
            try
            {
                if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                int NLn = m_Lines.GetLength(0);
                double[,] Lines = new double[NLn + 1, m_Lines.GetLength(1)];
                for (int i = 0; i < NLn; i++)
                {
                    for (int j = 0; j < m_Lines.GetLength(1); j++)
                    {
                        Lines[i, j] = m_Lines[i, j];
                    }
                }
                Lines[NLn, 0] = X1;
                Lines[NLn, 1] = Y1;
                Lines[NLn, 2] = X2;
                Lines[NLn, 3] = Y2;
                Lines[NLn, 8] = PenNum;
                m_Lines = Lines;
                RetCode = true;
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }
        public void SetWinSize(int WinX, int WinY)
        {
            m_Limits[1][0][1] = WinX;
            m_Limits[1][1][1] = WinY;
            SetZoomFactor();
            ResetPicture();
        }
        public void SetView(double Xmin, double Ymin, double Xmax, double Ymax)
        {
            double XL = Xmax - Xmin;
            double YL = Ymax - Ymin;
            m_Limits[0][0][0] = Xmin + XL / 2.0;
            m_Limits[0][1][0] = Ymin + YL / 2.0;
            m_Limits[0][0][1] = XL;
            m_Limits[0][1][1] = YL;
            SetZoomFactor();
        }
        public void SetZoomFactor()
        {
            double XL = m_Limits[0][0][1];
            double YL = m_Limits[0][1][1];
            double WinX = m_Limits[1][0][1];
            double WinY = m_Limits[1][1][1];

            double XFac = WinX / XL;
            double YFac = WinY / YL;
            m_Zoomfactor = Math.Min(XFac, YFac);
        }
        public void ZoomFit()
        {
            double Xmin, Xmax, Ymin, Ymax;
            int NPT = m_Points.GetLength(0);
            int NSPT = m_SymPoints.GetLength(0);
            int NLine = m_Lines.GetLength(0);
            if (NPT > 0)
            {
                Xmin = m_Points[0, 0];
                Xmax = m_Points[0, 0];
                Ymin = m_Points[0, 1];
                Ymax = m_Points[0, 1];
            }
            else if (NSPT > 0)
            {
                Xmin = m_SymPoints[0, 0];
                Xmax = m_SymPoints[0, 0];
                Ymin = m_SymPoints[0, 1];
                Ymax = m_SymPoints[0, 1];
            }
            else if (NLine > 0)
            {
                Xmin = m_Lines[0, 0];
                Xmax = m_Lines[0, 0];
                Ymin = m_Lines[0, 1];
                Ymax = m_Lines[0, 1];
            }
            else
            {
                return;
            }
            for (int i = 0; i < NPT; i++)
            {
                Xmin = Math.Min(Xmin, m_Points[i, 0]);
                Xmax = Math.Max(Xmax, m_Points[i, 0]);
                Ymin = Math.Min(Ymin, m_Points[i, 1]);
                Ymax = Math.Max(Ymax, m_Points[i, 1]);
            }
            for (int i = 0; i < NSPT; i++)
            {
                Xmin = Math.Min(Xmin, m_SymPoints[i, 0]);
                Xmax = Math.Max(Xmax, m_SymPoints[i, 0]);
                Ymin = Math.Min(Ymin, m_SymPoints[i, 1]);
                Ymax = Math.Max(Ymax, m_SymPoints[i, 1]);
            }
            for (int i = 0; i < NLine; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Xmin = Math.Min(Xmin, m_Lines[i, 0 + 2 * j]);
                    Xmax = Math.Max(Xmax, m_Lines[i, 0 + 2 * j]);
                    Ymin = Math.Min(Ymin, m_Lines[i, 1 + 2 * j]);
                    Ymax = Math.Max(Ymax, m_Lines[i, 1 + 2 * j]);
                }
            }
            double XL = 1.1 * (Xmax - Xmin); // add 10% margin
            double YL = 1.1 * (Ymax - Ymin);
            double Xo = 0.5 * (Xmin + Xmax);
            double Yo = 0.5 * (Ymin + Ymax);
            double Ltol = 1.0e-10;
            if (XL <= 0) XL = Ltol;
            if (YL <= 0) YL = Ltol;
            m_Limits[0][0][0] = Xo;
            m_Limits[0][1][0] = Yo;
            m_Limits[0][0][1] = XL;
            m_Limits[0][1][1] = YL;
            ResetPicture();
        }
        public void Zoom(double ratio)
        {
            m_Limits[0][0][1] *= ratio;
            m_Limits[0][1][1] *= ratio;
            ResetPicture();
        }
        public void pan(double dX, double dY, int UpdateFlag = 1)
        {
            m_Limits[0][0][0] += dX;  // Xo
            m_Limits[0][1][0] += dY;  // Yo
            ResetPicture();
            if (UpdateFlag == 0) { m_Limits[0][0][0] -= dX; m_Limits[0][1][0] -= dY; } // if no update -> reset the origin where it was
        }
        public void ResetPicture()
        {
            SetZoomFactor();
            for (int i = 0; i < m_Points.GetLength(0); i++)
            {
                double xleft = m_Points[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_Points[i, 1] - m_Limits[0][1][0]);
                m_Points[i, 2] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Points[i, 3] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
            for (int i = 0; i < m_SymPoints.GetLength(0); i++)
            {
                double xleft = m_SymPoints[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_SymPoints[i, 1] - m_Limits[0][1][0]);
                m_SymPoints[i, 2] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_SymPoints[i, 3] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                double xleft = m_Lines[i, 0] - m_Limits[0][0][0];
                double ytop = -(m_Lines[i, 1] - m_Limits[0][1][0]);
                m_Lines[i, 4] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Lines[i, 5] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
                xleft = m_Lines[i, 2] - m_Limits[0][0][0];
                ytop = -(m_Lines[i, 3] - m_Limits[0][1][0]);
                m_Lines[i, 6] = xleft * m_Zoomfactor + 0.5 * m_Limits[1][0][1];
                m_Lines[i, 7] = ytop * m_Zoomfactor + 0.5 * m_Limits[1][1][1];
            }
        }
        public void GetGraphic(ref Graphics g)
        {
            for (int i = 0; i < m_Points.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Points[i, 4]);
                int X = Convert.ToInt16(m_Points[i, 2]);
                int Y = Convert.ToInt16(m_Points[i, 3]);
                g.DrawRectangle(m_Pens[PenNum], X - 1, Y - 1, 2, 2);
            }
            for (int i = 0; i < m_SymPoints.GetLength(0); i++)
            {
                int SymNum = Convert.ToInt16(m_SymPoints[i, 4]);
                int[,] Symbol = m_Symbols[SymNum];
                int PenNum = Symbol[0, 0];
                int Scale = Symbol[0, 1];
                int NPt = Symbol.GetLength(0);
                int Xs = Convert.ToInt16(m_SymPoints[i, 2]);
                int Ys = Convert.ToInt16(m_SymPoints[i, 3]);
                int X1 = Xs + Scale * Symbol[NPt - 1, 0];
                int Y1 = Ys - Scale * Symbol[NPt - 1, 1];
                for (int p = 1; p < NPt; p++)
                {
                    int X2 = Xs + Scale * Symbol[p, 0];
                    int Y2 = Ys - Scale * Symbol[p, 1];
                    g.DrawLine(m_Pens[PenNum], X1, Y1, X2, Y2);
                    X1 = X2;
                    Y1 = Y2;
                }
            }
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines[i, 8]);
                int[] xPt = new int[2];
                int[] yPt = new int[2];
                for (int j = 0; j < 2; j++)
                {
                    xPt[j] = Convert.ToInt16(m_Lines[i, 2 * j + 4]);
                    yPt[j] = Convert.ToInt16(m_Lines[i, 2 * j + 5]);
                }
                g.DrawLine(m_Pens[PenNum], xPt[0], yPt[0], xPt[1], yPt[1]);
            }
        }
        public void AddSymbol(K2DSymbols SymName, int PenNum, int ScaleFactor)
        {
            int[,] SymPoints = null;
            switch (SymName)
            {
                case K2DSymbols.Star:
                    SymPoints = new int[6, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 2;
                    SymPoints[3, 0] = -1;
                    SymPoints[3, 1] = -2;
                    SymPoints[4, 0] = 2;
                    SymPoints[4, 1] = 1;
                    SymPoints[5, 0] = -2;
                    SymPoints[5, 1] = 1;
                    break;
                case K2DSymbols.Triangle:
                    SymPoints = new int[4, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 2;
                    SymPoints[1, 1] = -1;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 2;
                    SymPoints[3, 0] = -2;
                    SymPoints[3, 1] = -1;
                    break;
                case K2DSymbols.Hexagon:
                    SymPoints = new int[7, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 2;
                    SymPoints[2, 1] = 0;
                    SymPoints[3, 0] = 1;
                    SymPoints[3, 1] = 2;
                    SymPoints[4, 0] = -1;
                    SymPoints[4, 1] = 2;
                    SymPoints[5, 0] = -2;
                    SymPoints[5, 1] = 0;
                    SymPoints[6, 0] = -1;
                    SymPoints[6, 1] = -2;
                    break;
                case K2DSymbols.Burst:
                    SymPoints = new int[15, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 0;
                    SymPoints[1, 1] = 0;
                    SymPoints[2, 0] = 0;
                    SymPoints[2, 1] = 1;
                    SymPoints[3, 0] = 0;
                    SymPoints[3, 1] = 0;
                    SymPoints[4, 0] = -1;
                    SymPoints[4, 1] = 1;
                    SymPoints[5, 0] = 0;
                    SymPoints[5, 1] = 0;
                    SymPoints[6, 0] = -1;
                    SymPoints[6, 1] = 0;
                    SymPoints[7, 0] = 0;
                    SymPoints[7, 1] = 0;
                    SymPoints[8, 0] = -1;
                    SymPoints[8, 1] = -1;
                    SymPoints[9, 0] = 0;
                    SymPoints[9, 1] = 0;
                    SymPoints[10, 0] = 0;
                    SymPoints[10, 1] = -1;
                    SymPoints[11, 0] = 0;
                    SymPoints[11, 1] = 0;
                    SymPoints[12, 0] = 1;
                    SymPoints[12, 1] = -1;
                    SymPoints[13, 0] = 0;
                    SymPoints[13, 1] = 0;
                    SymPoints[14, 0] = 1;
                    SymPoints[14, 1] = 0;
                    SymPoints[13, 0] = 0;
                    SymPoints[13, 1] = 0;
                    SymPoints[14, 0] = 1;
                    SymPoints[14, 1] = 1;
                    break;
                case K2DSymbols.Octagon:
                    SymPoints = new int[9, 2];
                    if (PenNum >= m_Pens.Length || PenNum < 0) PenNum = 0;
                    SymPoints[0, 0] = PenNum;
                    SymPoints[0, 1] = ScaleFactor;
                    SymPoints[1, 0] = 1;
                    SymPoints[1, 1] = -2;
                    SymPoints[2, 0] = 2;
                    SymPoints[2, 1] = -1;
                    SymPoints[3, 0] = 2;
                    SymPoints[3, 1] = 1;
                    SymPoints[4, 0] = 1;
                    SymPoints[4, 1] = 2;
                    SymPoints[5, 0] = -1;
                    SymPoints[5, 1] = 2;
                    SymPoints[6, 0] = -2;
                    SymPoints[6, 1] = 1;
                    SymPoints[7, 0] = -2;
                    SymPoints[7, 1] = -1;
                    SymPoints[8, 0] = -1;
                    SymPoints[8, 1] = -2;
                    break;
            }
            int NSym = m_Symbols.Length;
            int[][,] Symbols = new int[NSym + 1][,];
            for (int i = 0; i < NSym; i++)
            {
                Symbols[i] = m_Symbols[i];
            }
            Symbols[NSym] = SymPoints;
            m_Symbols = Symbols;
        }

        public int[] GetLines(double X, double Y)
        {
            int[] Lines = new int[0]; ;
            for (int i = 0; i < m_Lines.GetLength(0); i++)
            {
                bool Online = false;
                int Tolerance = 4;
                double x1 = m_Lines[i, 4];
                double y1 = m_Lines[i, 5];
                double x2 = m_Lines[i, 6];
                double y2 = m_Lines[i, 7];
                if (x1 == x2)
                {
                    if (Math.Abs(X - x1) < Tolerance && (Y - y1) * (Y - y2) <= 0) Online = true;
                }
                else if ((X - x1) * (X - x2) <= 0)
                {
                    double m1 = (y2 - y1) / (x2 - x1);
                    double dL = 0;
                    if (m1 == 0)
                    {
                        dL = Math.Abs(Y - y1);
                    }
                    else
                    {
                        double m2 = -1 / m1;
                        double y01 = y1 - m1 * x1;
                        double y02 = Y - m2 * X;
                        double xIntersect = -(y02 - y01) / (m2 - m1);
                        double yIntersect = y01 + m1 * xIntersect;
                        dL = Math.Sqrt(Math.Pow((X - xIntersect), 2) + Math.Pow((Y - yIntersect), 2));
                    }
                    if (dL < Tolerance) Online = true;
                    //double yy = y1 + (X - x1) * (y2 - y1) / (x2 - x1);
                    //if (Math.Abs(yy - Y) < Tolerance) Online = true;
                }
                if (Online)
                {
                    int[] Lines2 = new int[Lines.Length + 1];
                    for (int j = 0; j < Lines.Length; j++) Lines2[j] = Lines[j];
                    Lines2[Lines.Length] = i;
                    Lines = Lines2;
                }

            }
            return Lines;
        }
    }
}
*/
