﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZoconFEA3D;                   // Add for anything that needs to access m_Structure or core functions

namespace CBridge
{
    public partial class frmAbutment : Form
    {
        bool DoEvents = false;                           // flag for turning event handling off and on selectively

        int m_Unit = 2;                                  // default for cmbUnit index for kip, inch
        string[,] m_HeaderText = new string[4, 5];        // one for each unit name like "ksi, inch"
        double[,] m_UnitFactor = null;                    // unit conversions from ksi to selected unit
        double[,] m_Limits = null;                        //  Min, max, default for each column
        private List<Abutment> m_AbutmentList;
        bool[] m_InUse_Conc = new bool[10];               // Flags for Abutment being Used , So we cant delete        
        public string[,] m_RenameListConc = null;         // List of Abutment new names returned with new list
        public bool m_OK = false;

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Constructor of Abutment form
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        public frmAbutment()
        {
            InitializeComponent();
            
            m_Limits = new double[4, 12];                    //  Min, max, default, unit conversion for each column 
                                                            // (units of ksi) {InName, NewName, In-Use, E, nu, alfa, gama)

            // Defaults are set for steel in ksi
            //      min                  max                        default                conversion 
            m_Limits[0, 5] = 0;  m_Limits[1, 5] = 1.0e+15;  m_Limits[2, 5] = 30;   m_Limits[3, 5] = 1;    // Skew 
            m_Limits[0, 6] = 0;  m_Limits[1, 6] = 1.0e+15;  m_Limits[2, 6] = 40;   m_Limits[3, 6] = 1;    // Kx
            m_Limits[0, 7] = 0;  m_Limits[1, 7] = 1.0e+15;  m_Limits[2, 7] = 50;   m_Limits[3, 7] = 1;    // Ky
            m_Limits[0, 8] = 0;  m_Limits[1, 8] = 1.0e+15;  m_Limits[2, 8] = 60;   m_Limits[3, 8] = 1;    // Kz
            m_Limits[0, 9] = 0;  m_Limits[1, 9] = 1.0e+15;  m_Limits[2, 9] = 70;   m_Limits[3, 9] = 1;    // Krx
            m_Limits[0, 10]= 0;  m_Limits[1, 10]= 1.0e+15;  m_Limits[2, 10]= 80;   m_Limits[3, 10]= 1;    // Kry
            m_Limits[0, 11]= 0;  m_Limits[1, 11]= 1.0e+15;  m_Limits[2, 11]= 90;   m_Limits[3, 11]= 1;    // Krz

            // Header Texts 
            string[,] HeaderText =
                                     {{"Kx(psi)", "Ky(pci)", "Kz(psi)", "Krx((psi)" , "Kry((psi)", "Krz((psi)"},
                                      {"Kx(psf)", "Ky(pcf)", "Kz(psf)", "Krx((psf)" , "Kry((psf)", "Krz((psf)"},
                                      {"Kx(ksi)", "Ky(kci)", "Kz(ksi)", "Krx((ksi)" , "Kry((ksi)", "Krz((ksi)"},
                                      {"Kx(ksf)", "Ky(kcf)", "Kz(ksf)", "Krx((ksf)" , "Kry((ksf)", "Krz((ksf)"}};

            m_HeaderText = HeaderText;
            
            // unit conversion array       
            //                          Skew          kx           ky             kz              krx            kry
            double[,] Unitfactor =   {{1000.0        ,1000.0     ,1000.0        ,1000.0         ,1000.0        ,1000        ,1000      },
                                      {(1000*144)    ,(1000*144) ,(1000*144)    ,(1000*144)     ,(1000*144)    ,(1000*144)  ,(1000*144)},
                                      {1.0           ,1.0        ,1.0           ,1.0            ,1.0           ,1.0         ,1.0       },
                                      {144.0         ,144.0      ,144.0         ,144.0          ,144.0         ,144.0       ,144.0     }};

            m_UnitFactor = Unitfactor;
            cmbUnit.Text = cmbUnit.Items[2].ToString(); // Initialize the dialog to kip, in
            cmbUnit.SelectedIndex = 2;
            m_Unit = 2;
            DoEvents = true; // a flag for turning event handling off and on selectively
        }

        // --------------------------------------------------------------------
        // User Functions
        // --------------------------------------------------------------------

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Grid view
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SetValues(Abutment[] iAbutment)
        {
            
            if (iAbutment != null)
            {
                UpdateGridViews(iAbutment);

                bool OK = true;     // = CheckAllLimits();  ///todo

                if (!OK) bOK.Enabled = false;
            }
        }

        /// <summary>
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private bool CheckAllLimits()
        {
            DataGridView dgv = null;

            bool AllOK = true;

            // for checkeing names we need a list of all materila names
            int totalNames = dgvAbutment.RowCount - 1;

            string[] AbutmentNames = new string[totalNames];

            int count = 0;

            dgv = dgvAbutment;
            
            for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
            {
                AbutmentNames[count] = Convert.ToString(dgv.Rows[iR].Cells[1].Value);
                count++;
            }
                                           
            string Message = "";

            for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
            {
                for (int iC = 0; iC < dgv.ColumnCount - 1; iC++)
                {
                    string Cellvalue = Convert.ToString(dgv.Rows[iR].Cells[iC].Value);

                    if (iC == 1)
                    {
                        // this is Name column, check against all names in all grids


                    }
                    else if (iC != 2) // do not check the In-Use Column
                    {

                        if (!CheckLimits(dgv, iR, iC, Cellvalue, ref Message))
                        {
                            dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                            dgv.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                            AllOK = false;
                        }
                        else
                        {
                            dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                            dgv.Rows[iR].Cells[iC].Style.BackColor = Color.White;
                        }
                    }
                }
            }
           
            return AllOK;
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Abutment Grid Views
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void UpdateGridViews(Abutment[] iAbutment)
        {
            DoEvents = false;

            if (iAbutment != null && iAbutment.Length > 0)
            {
                foreach (Abutment item in iAbutment)
                {
                    UpdateAbutmentRow((Abutment)item);
                }
            }

            DoEvents = true;
        }

        
        void UpdateAbutmentRow(Abutment item)
        {
            // Show Abutments        
            int i = 0;

            // Add a Row
            i = dgvAbutment.Rows.Add();

            dgvAbutment["InName", i].Value = item.Name;
            dgvAbutment["name", i].Value = item.DisplayName;    

            dgvAbutment["InUse", i].Value = "No";


            dgvAbutment["SeatTypes", i].Value   = item.Seattype;
            dgvAbutment["FixityTypes", i].Value = item.Fixitytype;

            dgvAbutment["Skew", i].Value = item.Skew;

            if (item.Fixitytype == "Spring")
            {
                dgvAbutment["Kx", i].Value = item.Kx;
                dgvAbutment["Ky", i].Value = item.Ky;
                dgvAbutment["Kz", i].Value = item.Kz;

                dgvAbutment["Krx", i].Value = item.Krx;
                dgvAbutment["Kry", i].Value = item.Kry;
                dgvAbutment["Krz", i].Value = item.Krz;
            }
            else
            {
                dgvAbutment["Kx", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Kx", i].ReadOnly = true;
                dgvAbutment["Ky", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Ky", i].ReadOnly = true;
                dgvAbutment["Kz", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Kz", i].ReadOnly = true;

                dgvAbutment["Krx", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Krx", i].ReadOnly = true;
                dgvAbutment["Kry", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Kry", i].ReadOnly = true;
                dgvAbutment["Krz", i].Style.BackColor = Color.LightGray;
                dgvAbutment["Krz", i].ReadOnly = true;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves Grid view data into structure parameters
        ///  
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SaveAbutments()
        {
            DoEvents = false;

            if (dgvAbutment.Rows.Count > 1)
            {
                SaveAbutment();
            }
        
            DoEvents = true;
        }

         
        void SaveAbutment()
        {
            // Make Generate names  
            DataGridView dgv = dgvAbutment;
       
            int count = dgv.Rows.Count - 1;

            m_AbutmentList = new List<Abutment>(count);

            int NumRename = 0;
            double Factor = 1;

            for (int i = 0; i < count; i++)
            {
                string AbutmentName = "";
               
                if (dgv[0, i].Value != null) AbutmentName = dgv[0, i].Value.ToString();

                if (AbutmentName== "") AbutmentName= dgv[1, i].Value.ToString();

                string Seattype   = dgv[3, i].Value.ToString();
                string Fixitytype = dgv[4, i].Value.ToString();

                double[] Prop = new double[7];

                for (int j = 5; j < 12; j++)
                {
                    int jj = j - 5;

                    Factor = m_UnitFactor[m_Unit, jj];

                    // some cell may have null if fixity is not set to "Spring"
                    if (dgv[j, i].Value != "")
                    {
                        Prop[j - 5] = Convert.ToDouble(dgv[j, i].Value) / Factor;
                    }
                    else
                    {
                        Prop[j - 5] = 0;
                    }
                }

                Abutment Abut1 = new Abutment(AbutmentName, AbutmentName, null , AbutmentName, Seattype , Fixitytype , Prop);

             // public Abutment(string name, string bearingid, Node3D abutnode, string abutSection, double skew)
                             
                Abut1.DisplayName = AbutmentName;

                m_AbutmentList.Add(Abut1);

                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv["InName", i].Value != null)
                {
                    if (dgv["InName", i].Value.ToString() != dgv["name", i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use Abutment
                dgv["InUse", i].Value = "NO";
            }

            // prepare list of renamed items
            m_RenameListConc = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < count; i++)
            {
                if (dgv["InName", i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv["InName", i].Value.ToString() != dgv["Name", i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListConc[iRename, 0] = dgv["InName", i].Value.ToString();
                        m_RenameListConc[iRename, 1] = dgv["Name", i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }

        #region ButtonEvents

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Replicat Rows of Data Grid  
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void ReplicateRow(DataGridView dgv, int RIndex)
        {
            bool EventStatus = DoEvents;

            DoEvents = false;

            int iRow = dgv.Rows.Add(); // The newly added row index is the last row

            //   string baseName = dgv[0, RIndex].Value.ToString();

            string baseName = "New_0";

            string[] NameList = new string[dgv.Rows.Count - 2];

            for (int i = 0; i < dgv.Rows.Count - 2; i++)
                NameList[i] = dgv[1, i].Value.ToString();

            string newName = GetName(baseName, NameList);

            dgv[0, iRow].Value = newName;
            dgv[1, iRow].Value = newName;

            dgv[2, iRow].Value = "No";

            for (int j = 3; j < dgv.Columns.Count; j++)
            {
                dgv[j, iRow].Value = dgv[j, RIndex].Value;
            }

            DoEvents = EventStatus;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Get Name 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private string GetName(string BaseName, string[] NameList)
        {
            IncrementName:

            string NewName = BaseName.Substring(0, BaseName.Length - 1);
            string right = BaseName.Substring(BaseName.Length - 1); // rightmost charachter

            try
            {
                int n = Convert.ToInt16(right);

                if (n == 9) NewName = NewName + "10";

                else NewName = NewName + Convert.ToString(n + 1);
            }
            catch

            {
                NewName = BaseName + "0";
            }

            for (int i = 0; i < NameList.Length; i++) // Check for duplicate names
            {
                if (NameList[i] == NewName)
                {
                    BaseName = NewName;

                    goto IncrementName;
                }
            }
            return NewName;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Event Functions 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Replicating Rows 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void dgvAbutment_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DoEvents)
            {
                DoEvents = false;

                DataGridView dgv = dgvAbutment;
                int i = dgv.CurrentRow.Index;

                string baseName = "New";
                string[] NameList = new string[dgv.Rows.Count - 2];

                for (int j = 0; j < dgv.Rows.Count - 2; j++) NameList[j] = dgv[1, j].Value.ToString();
                string newName = GetName(baseName, NameList);

                dgv[1, i].Value = newName;
                dgv[2, i].Value = "No";
                dgv[3, i].Value = "Seat";
                dgv[4, i].Value = "Spring";    
                
                if (i == 0)
                {
                    for (int j = 5; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, 0].Value = m_Limits[2, j];  // use default values from m_Limits column 2
                    }
                }
                else
                {
                    for (int j = 5; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, i].Value = dgv[j, i - 1].Value;
                    }
                }

                DoEvents = true;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Button delete and buttomn OKs 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void bDelete_Click(object sender, EventArgs e)
        {
            DataGridView dgv = dgvAbutment; 
          
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgv.SelectedRows)
                {
                    if (item.Index != dgv.Rows.Count - 1)
                    {
                        if (item.Cells[2].Value.ToString() == "No") dgv.Rows.Remove(item);
                    }
                }
            }

            CheckAllLimits();
        }

        private void bReplicate_Click(object sender, EventArgs e)
        {
           
            DataGridView dgv = dgvAbutment;

            foreach (DataGridViewRow item in dgv.SelectedRows)
            {
                if (item.Index != dgv.Rows.Count - 1) ReplicateRow(dgv, item.Index);
            }
        }


        private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!DoEvents) return;

            DoEvents = false;

            // Convert Concrete values
            dgvAbutment.Columns[3].DefaultCellStyle.Format = "#.###";

            int SelectedUnit = cmbUnit.SelectedIndex;

            ConvertCellUnits(dgvAbutment, SelectedUnit);

         
            m_Unit = SelectedUnit;

            DoEvents = true;
        }

        void ConvertCellUnits(DataGridView dgv, int SelectedUnit)
        {

            string GridName = dgv.Name;
            string[,] HeaderText = null;

            HeaderText = m_HeaderText;

            int LastColumn = dgv.ColumnCount;

            for (int i = 3; i < LastColumn; i++)
            {
                dgv.Columns[i].HeaderText = HeaderText[SelectedUnit, i - 3];
            }

            for (int j = 3; j < LastColumn; j++)
            {
                int jj = j - 3;

                double Factor = m_UnitFactor[SelectedUnit, jj] / m_UnitFactor[m_Unit, jj];

                m_Limits[3, j] = m_Limits[3, j] * Factor;

                for (int ir = 0; ir < dgv.Rows.Count - 1; ir++)
                {
                    dgv[j, ir].Value = (Convert.ToDouble(dgv[j, ir].Value)) * Factor;
                }
            }
        }
        
      
      
        private void bCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // Reset the Abutment List
            SaveAbutments();

            //Close;
            m_OK = true;
            this.Close();
            return;
        }

        public void UpdateBridgeInfo(ref Abutment[] iAbutment)
        {
            int num = 0;
            if (m_AbutmentList != null) num += m_AbutmentList.Count;
            
            if (num > 0)
            {
                iAbutment = new Abutment[num];

                int i = 0;

                if (m_AbutmentList != null)
                {
                    foreach (Abutment item in m_AbutmentList)
                    {
                        iAbutment[i++] = item;
                    }
                }                 
            }
        }
        
        /////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  When any Cell value change, we check all cell to make sure the are withing the set limits
        /// 
        /// 
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        private void dgvAbutment_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvAbutment.CurrentRow == null) return;

                string Message = "";

                int Row = dgvAbutment.CurrentRow.Index;

                int Col = dgvAbutment.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvAbutment.CurrentCell.Value);

                if (!CheckLimits(dgvAbutment, Row, Col, Cellvalue, ref Message))
                {
                    dgvAbutment.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvAbutment.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    bOK.Enabled = true; //CheckAllLimits(); // Temporary; We do not need the check after this

                }
            }
        }

     
        private bool CheckLimits(DataGridView dgv, int Row, int Column, string Value, ref string Message)
        {
            bool RetCode = true;
            try
            {
                int caseCode = 0; // We never need to check InName

                if (Column == 1) caseCode = 1; // Name

                if (Column == 2)
                {
                    System.Diagnostics.Debugger.Break();
                }

                if (Column == 4)
                {
                    if (Value != "Spring")
                    {
                        // disable and gray out all kx and krx cells in this row
                        for (int i = 6; i < 12; i++)
                        {
                            dgvAbutment.Rows[Row].Cells[i].Value = "";
                            dgvAbutment.Rows[Row].Cells[i].Style.BackColor = Color.LightGray;
                            dgvAbutment.Rows[Row].Cells[i].ReadOnly = true;
                        }
                    }
                    else
                    {
                        // enable all kx and krx cells in this row
                        for (int i = 6; i < 12; i++)
                        {
                            dgvAbutment.Rows[Row].Cells[i].Style.BackColor = Color.White;
                            dgvAbutment.Rows[Row].Cells[i].ReadOnly = false;
                        }
                    }
                }
                                
                if (Column > 4) caseCode = 2; // Col 3 and 4 are drop downs and dont need to check

                switch (caseCode)
                {
                    case 1:  // Check for duplicate names
                        for (int i = 0; i < dgv.Rows.Count - 1; i++)
                        {
                            string CheckName = dgv[1, i].Value.ToString();

                            if (Value == CheckName && i != Row)
                            {
                                RetCode = false;
                                Message = "Duplicate Name";
                                break;
                            }
                        }
                        break;

                    case 2:  // Check Limits

                        double dValue = Convert.ToDouble(Value);

                        if (dValue < m_Limits[0, Column] || dValue > m_Limits[1, Column])
                        {
                            RetCode = false;
                            Message = "Out of Range";
                            break;
                        }
                        break;

                    default:

                        break;
                }
            }
            catch
            {
                RetCode = false;
                Message = "Limit Check Exception";
            }
            return RetCode;
        }

        private void dgvAbutment_SelectionChanged(object sender, EventArgs e)
        {

            if (DoEvents)
            {
                DoEvents = false;

                if (dgvAbutment.SelectedRows.Count > 0)
                {
                    bDelete.Enabled = true;
                    bReplicate.Enabled = true;
                }
                else
                {
                    bDelete.Enabled = false;
                    bReplicate.Enabled = false;
                }

                DoEvents = true;
            }
        }

        #endregion ButtonEvents
    }
}
