﻿namespace CBridge
{
    partial class frmMemberSections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMemberSections));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PicRectangle = new System.Windows.Forms.PictureBox();
            this.PictureZoom = new System.Windows.Forms.PictureBox();
            this.PicBathTub = new System.Windows.Forms.PictureBox();
            this.PicVoidedSlab = new System.Windows.Forms.PictureBox();
            this.PicIGirder = new System.Windows.Forms.PictureBox();
            this.PicBoxGirder = new System.Windows.Forms.PictureBox();
            this.PicUGirder = new System.Windows.Forms.PictureBox();
            this.PicPrecastBox = new System.Windows.Forms.PictureBox();
            this.PicCircle = new System.Windows.Forms.PictureBox();
            this.cmbSelType = new System.Windows.Forms.ComboBox();
            this.pbModel = new System.Windows.Forms.PictureBox();
            this.cmbUnitRectangle = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnLibSection = new System.Windows.Forms.Button();
            this.chbInUse = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnProperties = new System.Windows.Forms.Button();
            this.dgvRectangle = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCircle = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvIGirder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvBoxGirder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvUGirder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPrecastBox = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvVoidedSlab = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCABathTub = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chbCSRectangle = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbUnitCircle = new System.Windows.Forms.ComboBox();
            this.cmbUnitIGirder = new System.Windows.Forms.ComboBox();
            this.cmbUnitBoxGirder = new System.Windows.Forms.ComboBox();
            this.cmbUnitUGirder = new System.Windows.Forms.ComboBox();
            this.cmbUnitPrecastBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicRectangle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBathTub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicVoidedSlab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicIGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPrecastBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCircle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRectangle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCircle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUGirder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecastBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoidedSlab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCABathTub)).BeginInit();
            this.SuspendLayout();
            // 
            // PicRectangle
            // 
            this.PicRectangle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicRectangle.Cursor = System.Windows.Forms.Cursors.Default;
            this.PicRectangle.Image = ((System.Drawing.Image)(resources.GetObject("PicRectangle.Image")));
            this.PicRectangle.Location = new System.Drawing.Point(627, 73);
            this.PicRectangle.Name = "PicRectangle";
            this.PicRectangle.Size = new System.Drawing.Size(272, 177);
            this.PicRectangle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRectangle.TabIndex = 83;
            this.PicRectangle.TabStop = false;
            // 
            // PictureZoom
            // 
            this.PictureZoom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PictureZoom.Cursor = System.Windows.Forms.Cursors.Default;
            this.PictureZoom.Location = new System.Drawing.Point(627, 73);
            this.PictureZoom.Name = "PictureZoom";
            this.PictureZoom.Size = new System.Drawing.Size(272, 177);
            this.PictureZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureZoom.TabIndex = 91;
            this.PictureZoom.TabStop = false;
            this.PictureZoom.Visible = false;
            // 
            // PicBathTub
            // 
            this.PicBathTub.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicBathTub.Image = ((System.Drawing.Image)(resources.GetObject("PicBathTub.Image")));
            this.PicBathTub.Location = new System.Drawing.Point(627, 73);
            this.PicBathTub.Name = "PicBathTub";
            this.PicBathTub.Size = new System.Drawing.Size(272, 177);
            this.PicBathTub.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBathTub.TabIndex = 90;
            this.PicBathTub.TabStop = false;
            this.PicBathTub.Visible = false;
            // 
            // PicVoidedSlab
            // 
            this.PicVoidedSlab.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicVoidedSlab.Image = ((System.Drawing.Image)(resources.GetObject("PicVoidedSlab.Image")));
            this.PicVoidedSlab.Location = new System.Drawing.Point(627, 73);
            this.PicVoidedSlab.Name = "PicVoidedSlab";
            this.PicVoidedSlab.Size = new System.Drawing.Size(272, 177);
            this.PicVoidedSlab.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicVoidedSlab.TabIndex = 89;
            this.PicVoidedSlab.TabStop = false;
            this.PicVoidedSlab.Visible = false;
            // 
            // PicIGirder
            // 
            this.PicIGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicIGirder.Image = ((System.Drawing.Image)(resources.GetObject("PicIGirder.Image")));
            this.PicIGirder.Location = new System.Drawing.Point(627, 73);
            this.PicIGirder.Name = "PicIGirder";
            this.PicIGirder.Size = new System.Drawing.Size(272, 177);
            this.PicIGirder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicIGirder.TabIndex = 85;
            this.PicIGirder.TabStop = false;
            this.PicIGirder.Visible = false;
            // 
            // PicBoxGirder
            // 
            this.PicBoxGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicBoxGirder.Image = ((System.Drawing.Image)(resources.GetObject("PicBoxGirder.Image")));
            this.PicBoxGirder.Location = new System.Drawing.Point(627, 73);
            this.PicBoxGirder.Name = "PicBoxGirder";
            this.PicBoxGirder.Size = new System.Drawing.Size(272, 177);
            this.PicBoxGirder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBoxGirder.TabIndex = 86;
            this.PicBoxGirder.TabStop = false;
            this.PicBoxGirder.Visible = false;
            // 
            // PicUGirder
            // 
            this.PicUGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicUGirder.Image = ((System.Drawing.Image)(resources.GetObject("PicUGirder.Image")));
            this.PicUGirder.Location = new System.Drawing.Point(627, 73);
            this.PicUGirder.Name = "PicUGirder";
            this.PicUGirder.Size = new System.Drawing.Size(272, 177);
            this.PicUGirder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicUGirder.TabIndex = 87;
            this.PicUGirder.TabStop = false;
            this.PicUGirder.Visible = false;
            // 
            // PicPrecastBox
            // 
            this.PicPrecastBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicPrecastBox.Image = ((System.Drawing.Image)(resources.GetObject("PicPrecastBox.Image")));
            this.PicPrecastBox.Location = new System.Drawing.Point(627, 73);
            this.PicPrecastBox.Name = "PicPrecastBox";
            this.PicPrecastBox.Size = new System.Drawing.Size(272, 177);
            this.PicPrecastBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPrecastBox.TabIndex = 88;
            this.PicPrecastBox.TabStop = false;
            this.PicPrecastBox.Visible = false;
            // 
            // PicCircle
            // 
            this.PicCircle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PicCircle.Image = ((System.Drawing.Image)(resources.GetObject("PicCircle.Image")));
            this.PicCircle.Location = new System.Drawing.Point(627, 73);
            this.PicCircle.Name = "PicCircle";
            this.PicCircle.Size = new System.Drawing.Size(272, 177);
            this.PicCircle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCircle.TabIndex = 84;
            this.PicCircle.TabStop = false;
            this.PicCircle.Visible = false;
            // 
            // cmbSelType
            // 
            this.cmbSelType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbSelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelType.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbSelType.FormattingEnabled = true;
            this.cmbSelType.Items.AddRange(new object[] {
            "Rectangle",
            "Circle",
            "I-Girder",
            "Box Girder",
            "U-Girder",
            "Precast Box",
            "Voided Slab",
            "CA BathTub",
            "Parts",
            "General"});
            this.cmbSelType.Location = new System.Drawing.Point(42, 13);
            this.cmbSelType.Name = "cmbSelType";
            this.cmbSelType.Size = new System.Drawing.Size(97, 21);
            this.cmbSelType.TabIndex = 82;
            this.cmbSelType.SelectedIndexChanged += new System.EventHandler(this.cmbSelType_SelectedIndexChanged);
            // 
            // pbModel
            // 
            this.pbModel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbModel.BackColor = System.Drawing.Color.White;
            this.pbModel.Location = new System.Drawing.Point(241, 73);
            this.pbModel.Name = "pbModel";
            this.pbModel.Size = new System.Drawing.Size(380, 380);
            this.pbModel.TabIndex = 81;
            this.pbModel.TabStop = false;
            // 
            // cmbUnitRectangle
            // 
            this.cmbUnitRectangle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitRectangle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitRectangle.FormattingEnabled = true;
            this.cmbUnitRectangle.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitRectangle.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitRectangle.Name = "cmbUnitRectangle";
            this.cmbUnitRectangle.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitRectangle.TabIndex = 80;
            this.cmbUnitRectangle.SelectedIndexChanged += new System.EventHandler(this.cmbUnitRectangle_SelectedIndexChanged);
            this.cmbUnitRectangle.SelectedValueChanged += new System.EventHandler(this.cmbUnitRectangle_SelectedValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(824, 534);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 77;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(743, 534);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 76;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnLibSection
            // 
            this.btnLibSection.Location = new System.Drawing.Point(123, 534);
            this.btnLibSection.Name = "btnLibSection";
            this.btnLibSection.Size = new System.Drawing.Size(105, 23);
            this.btnLibSection.TabIndex = 74;
            this.btnLibSection.Text = "Library Section";
            this.btnLibSection.UseVisualStyleBackColor = true;
            this.btnLibSection.Click += new System.EventHandler(this.btnLibSection_Click);
            // 
            // chbInUse
            // 
            this.chbInUse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chbInUse.AutoSize = true;
            this.chbInUse.Location = new System.Drawing.Point(356, 50);
            this.chbInUse.Name = "chbInUse";
            this.chbInUse.Size = new System.Drawing.Size(78, 17);
            this.chbInUse.TabIndex = 73;
            this.chbInUse.Text = "In Use Yes";
            this.chbInUse.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Type:";
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(12, 534);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(105, 23);
            this.btnProperties.TabIndex = 75;
            this.btnProperties.Text = "Properties";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // dgvRectangle
            // 
            this.dgvRectangle.AllowUserToAddRows = false;
            this.dgvRectangle.AllowUserToDeleteRows = false;
            this.dgvRectangle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvRectangle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvRectangle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRectangle.ColumnHeadersVisible = false;
            this.dgvRectangle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgvRectangle.Location = new System.Drawing.Point(5, 50);
            this.dgvRectangle.Name = "dgvRectangle";
            this.dgvRectangle.RowHeadersVisible = false;
            this.dgvRectangle.Size = new System.Drawing.Size(230, 157);
            this.dgvRectangle.TabIndex = 71;
            this.dgvRectangle.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRectangle_CellEndEdit);
            // 
            // Column1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "clmDim";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "clmValue";
            this.Column2.Name = "Column2";
            this.Column2.Width = 127;
            // 
            // dgvCircle
            // 
            this.dgvCircle.AllowUserToAddRows = false;
            this.dgvCircle.AllowUserToDeleteRows = false;
            this.dgvCircle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvCircle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCircle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCircle.ColumnHeadersVisible = false;
            this.dgvCircle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvCircle.Location = new System.Drawing.Point(5, 50);
            this.dgvCircle.Name = "dgvCircle";
            this.dgvCircle.RowHeadersVisible = false;
            this.dgvCircle.Size = new System.Drawing.Size(230, 25);
            this.dgvCircle.TabIndex = 93;
            this.dgvCircle.Visible = false;
            this.dgvCircle.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCircle_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 127;
            // 
            // dgvIGirder
            // 
            this.dgvIGirder.AllowUserToAddRows = false;
            this.dgvIGirder.AllowUserToDeleteRows = false;
            this.dgvIGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvIGirder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvIGirder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIGirder.ColumnHeadersVisible = false;
            this.dgvIGirder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgvIGirder.Location = new System.Drawing.Point(5, 50);
            this.dgvIGirder.Name = "dgvIGirder";
            this.dgvIGirder.RowHeadersVisible = false;
            this.dgvIGirder.Size = new System.Drawing.Size(230, 333);
            this.dgvIGirder.TabIndex = 94;
            this.dgvIGirder.Visible = false;
            this.dgvIGirder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIGirder_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 127;
            // 
            // dgvBoxGirder
            // 
            this.dgvBoxGirder.AllowUserToAddRows = false;
            this.dgvBoxGirder.AllowUserToDeleteRows = false;
            this.dgvBoxGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvBoxGirder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBoxGirder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBoxGirder.ColumnHeadersVisible = false;
            this.dgvBoxGirder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.dgvBoxGirder.Location = new System.Drawing.Point(5, 50);
            this.dgvBoxGirder.Name = "dgvBoxGirder";
            this.dgvBoxGirder.RowHeadersVisible = false;
            this.dgvBoxGirder.Size = new System.Drawing.Size(230, 443);
            this.dgvBoxGirder.TabIndex = 95;
            this.dgvBoxGirder.Visible = false;
            this.dgvBoxGirder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBoxGirder_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 127;
            // 
            // dgvUGirder
            // 
            this.dgvUGirder.AllowUserToAddRows = false;
            this.dgvUGirder.AllowUserToDeleteRows = false;
            this.dgvUGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvUGirder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvUGirder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUGirder.ColumnHeadersVisible = false;
            this.dgvUGirder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgvUGirder.Location = new System.Drawing.Point(5, 50);
            this.dgvUGirder.Name = "dgvUGirder";
            this.dgvUGirder.RowHeadersVisible = false;
            this.dgvUGirder.Size = new System.Drawing.Size(230, 311);
            this.dgvUGirder.TabIndex = 96;
            this.dgvUGirder.Visible = false;
            this.dgvUGirder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUGirder_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 127;
            // 
            // dgvPrecastBox
            // 
            this.dgvPrecastBox.AllowUserToAddRows = false;
            this.dgvPrecastBox.AllowUserToDeleteRows = false;
            this.dgvPrecastBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvPrecastBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPrecastBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrecastBox.ColumnHeadersVisible = false;
            this.dgvPrecastBox.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dgvPrecastBox.Location = new System.Drawing.Point(5, 50);
            this.dgvPrecastBox.Name = "dgvPrecastBox";
            this.dgvPrecastBox.RowHeadersVisible = false;
            this.dgvPrecastBox.Size = new System.Drawing.Size(230, 245);
            this.dgvPrecastBox.TabIndex = 97;
            this.dgvPrecastBox.Visible = false;
            this.dgvPrecastBox.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrecastBox_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn9.Frozen = true;
            this.dataGridViewTextBoxColumn9.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 127;
            // 
            // dgvVoidedSlab
            // 
            this.dgvVoidedSlab.AllowUserToAddRows = false;
            this.dgvVoidedSlab.AllowUserToDeleteRows = false;
            this.dgvVoidedSlab.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvVoidedSlab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvVoidedSlab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVoidedSlab.ColumnHeadersVisible = false;
            this.dgvVoidedSlab.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dgvVoidedSlab.Location = new System.Drawing.Point(5, 50);
            this.dgvVoidedSlab.Name = "dgvVoidedSlab";
            this.dgvVoidedSlab.RowHeadersVisible = false;
            this.dgvVoidedSlab.Size = new System.Drawing.Size(230, 267);
            this.dgvVoidedSlab.TabIndex = 98;
            this.dgvVoidedSlab.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn11.Frozen = true;
            this.dataGridViewTextBoxColumn11.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 127;
            // 
            // dgvCABathTub
            // 
            this.dgvCABathTub.AllowUserToAddRows = false;
            this.dgvCABathTub.AllowUserToDeleteRows = false;
            this.dgvCABathTub.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvCABathTub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCABathTub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCABathTub.ColumnHeadersVisible = false;
            this.dgvCABathTub.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            this.dgvCABathTub.Location = new System.Drawing.Point(5, 50);
            this.dgvCABathTub.Name = "dgvCABathTub";
            this.dgvCABathTub.RowHeadersVisible = false;
            this.dgvCABathTub.Size = new System.Drawing.Size(230, 355);
            this.dgvCABathTub.TabIndex = 99;
            this.dgvCABathTub.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn13.Frozen = true;
            this.dataGridViewTextBoxColumn13.HeaderText = "clmDim";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "clmValue";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 127;
            // 
            // chbCSRectangle
            // 
            this.chbCSRectangle.AutoSize = true;
            this.chbCSRectangle.Enabled = false;
            this.chbCSRectangle.Location = new System.Drawing.Point(251, 50);
            this.chbCSRectangle.Name = "chbCSRectangle";
            this.chbCSRectangle.Size = new System.Drawing.Size(99, 17);
            this.chbCSRectangle.TabIndex = 100;
            this.chbCSRectangle.Text = "Composite Slab";
            this.chbCSRectangle.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(162, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Unit:";
            // 
            // cmbUnitCircle
            // 
            this.cmbUnitCircle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitCircle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitCircle.FormattingEnabled = true;
            this.cmbUnitCircle.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitCircle.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitCircle.Name = "cmbUnitCircle";
            this.cmbUnitCircle.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitCircle.TabIndex = 102;
            this.cmbUnitCircle.Visible = false;
            this.cmbUnitCircle.SelectedIndexChanged += new System.EventHandler(this.cmbUnitCircle_SelectedIndexChanged);
            this.cmbUnitCircle.SelectedValueChanged += new System.EventHandler(this.cmbUnitCircle_SelectedValueChanged_1);
            // 
            // cmbUnitIGirder
            // 
            this.cmbUnitIGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitIGirder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitIGirder.FormattingEnabled = true;
            this.cmbUnitIGirder.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitIGirder.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitIGirder.Name = "cmbUnitIGirder";
            this.cmbUnitIGirder.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitIGirder.TabIndex = 103;
            this.cmbUnitIGirder.Visible = false;
            this.cmbUnitIGirder.SelectedIndexChanged += new System.EventHandler(this.cmbUnitIGirder_SelectedIndexChanged);
            this.cmbUnitIGirder.SelectedValueChanged += new System.EventHandler(this.cmbUnitIGirder_SelectedValueChanged);
            // 
            // cmbUnitBoxGirder
            // 
            this.cmbUnitBoxGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitBoxGirder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitBoxGirder.FormattingEnabled = true;
            this.cmbUnitBoxGirder.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitBoxGirder.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitBoxGirder.Name = "cmbUnitBoxGirder";
            this.cmbUnitBoxGirder.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitBoxGirder.TabIndex = 104;
            this.cmbUnitBoxGirder.Visible = false;
            this.cmbUnitBoxGirder.SelectedIndexChanged += new System.EventHandler(this.cmbUnitBoxGirder_SelectedIndexChanged);
            this.cmbUnitBoxGirder.SelectedValueChanged += new System.EventHandler(this.cmbUnitBoxGirder_SelectedValueChanged);
            // 
            // cmbUnitUGirder
            // 
            this.cmbUnitUGirder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitUGirder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitUGirder.FormattingEnabled = true;
            this.cmbUnitUGirder.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitUGirder.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitUGirder.Name = "cmbUnitUGirder";
            this.cmbUnitUGirder.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitUGirder.TabIndex = 105;
            this.cmbUnitUGirder.Visible = false;
            this.cmbUnitUGirder.SelectedIndexChanged += new System.EventHandler(this.cmbUnitUGirder_SelectedIndexChanged);
            this.cmbUnitUGirder.SelectedValueChanged += new System.EventHandler(this.cmbUnitUGirder_SelectedValueChanged);
            // 
            // cmbUnitPrecastBox
            // 
            this.cmbUnitPrecastBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnitPrecastBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitPrecastBox.FormattingEnabled = true;
            this.cmbUnitPrecastBox.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnitPrecastBox.Location = new System.Drawing.Point(197, 13);
            this.cmbUnitPrecastBox.Name = "cmbUnitPrecastBox";
            this.cmbUnitPrecastBox.Size = new System.Drawing.Size(38, 21);
            this.cmbUnitPrecastBox.TabIndex = 106;
            this.cmbUnitPrecastBox.Visible = false;
            this.cmbUnitPrecastBox.SelectedIndexChanged += new System.EventHandler(this.cmbUnitPrecastBox_SelectedIndexChanged);
            this.cmbUnitPrecastBox.SelectedValueChanged += new System.EventHandler(this.cmbUnitPrecastBox_SelectedValueChanged);
            // 
            // frmMemberSections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 571);
            this.Controls.Add(this.dgvRectangle);
            this.Controls.Add(this.cmbUnitPrecastBox);
            this.Controls.Add(this.cmbUnitUGirder);
            this.Controls.Add(this.cmbUnitBoxGirder);
            this.Controls.Add(this.cmbUnitIGirder);
            this.Controls.Add(this.cmbUnitCircle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvVoidedSlab);
            this.Controls.Add(this.dgvPrecastBox);
            this.Controls.Add(this.dgvUGirder);
            this.Controls.Add(this.dgvBoxGirder);
            this.Controls.Add(this.dgvCircle);
            this.Controls.Add(this.chbCSRectangle);
            this.Controls.Add(this.dgvCABathTub);
            this.Controls.Add(this.dgvIGirder);
            this.Controls.Add(this.PicRectangle);
            this.Controls.Add(this.PictureZoom);
            this.Controls.Add(this.PicBathTub);
            this.Controls.Add(this.PicVoidedSlab);
            this.Controls.Add(this.PicIGirder);
            this.Controls.Add(this.PicBoxGirder);
            this.Controls.Add(this.PicUGirder);
            this.Controls.Add(this.PicPrecastBox);
            this.Controls.Add(this.PicCircle);
            this.Controls.Add(this.cmbSelType);
            this.Controls.Add(this.pbModel);
            this.Controls.Add(this.cmbUnitRectangle);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnLibSection);
            this.Controls.Add(this.chbInUse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnProperties);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMemberSections";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Member Sections";
            this.Load += new System.EventHandler(this.frmMemberSections_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicRectangle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBathTub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicVoidedSlab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicIGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPrecastBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCircle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRectangle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCircle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUGirder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecastBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoidedSlab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCABathTub)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox PicRectangle;
        internal System.Windows.Forms.PictureBox PictureZoom;
        internal System.Windows.Forms.PictureBox PicBathTub;
        internal System.Windows.Forms.PictureBox PicVoidedSlab;
        internal System.Windows.Forms.PictureBox PicIGirder;
        internal System.Windows.Forms.PictureBox PicBoxGirder;
        internal System.Windows.Forms.PictureBox PicUGirder;
        internal System.Windows.Forms.PictureBox PicPrecastBox;
        internal System.Windows.Forms.PictureBox PicCircle;
        private System.Windows.Forms.PictureBox pbModel;
        private System.Windows.Forms.ComboBox cmbUnitRectangle;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnLibSection;
        private System.Windows.Forms.CheckBox chbInUse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnProperties;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.CheckBox chbCSRectangle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbUnitCircle;
        private System.Windows.Forms.ComboBox cmbUnitIGirder;
        private System.Windows.Forms.ComboBox cmbUnitBoxGirder;
        private System.Windows.Forms.ComboBox cmbUnitUGirder;
        private System.Windows.Forms.ComboBox cmbUnitPrecastBox;
        public System.Windows.Forms.ComboBox cmbSelType;
        public System.Windows.Forms.DataGridView dgvRectangle;
        public System.Windows.Forms.DataGridView dgvCircle;
        public System.Windows.Forms.DataGridView dgvIGirder;
        public System.Windows.Forms.DataGridView dgvBoxGirder;
        public System.Windows.Forms.DataGridView dgvUGirder;
        public System.Windows.Forms.DataGridView dgvPrecastBox;
        public System.Windows.Forms.DataGridView dgvVoidedSlab;
        public System.Windows.Forms.DataGridView dgvCABathTub;
        private System.Windows.Forms.Button btnOK;
    }
}